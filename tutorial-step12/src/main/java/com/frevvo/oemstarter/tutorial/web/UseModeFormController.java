package com.frevvo.oemstarter.tutorial.web;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.Part;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.frevvo.forms.api.FormData;
import com.frevvo.forms.client.ApplicationEntry;
import com.frevvo.forms.client.ApplicationFeed;
import com.frevvo.forms.client.EntryHolder;
import com.frevvo.forms.client.FormEntry;
import com.frevvo.forms.client.FormFeed;
import com.frevvo.forms.client.FormTypeEntry;
import com.frevvo.forms.client.FormTypeFeed;
import com.frevvo.forms.client.FormsService;
import com.frevvo.forms.client.Helper;
import com.google.gdata.data.MediaContent;
import com.google.gdata.data.media.MediaSource;
import com.google.gdata.data.media.MediaStreamSource;

@Controller
@Scope("session")
public class UseModeFormController {

	static final private String LAST_FORM_DATA = "frevvo.forms.last.form.data";
	
	private EntryHolder instanceHolder;
	
    @RequestMapping(value = "/testForm", method = RequestMethod.GET)
    public String testFormSelectionPage(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String lastFormName = request.getParameter("lastFormName");
    	if (lastFormName == null)
    		lastFormName = "";
        model.addAttribute("lastFormName", lastFormName);
        return "test-form-selection";
    }

    @RequestMapping(value = "/testForm", method = RequestMethod.POST)
    public String testForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	String formtypeCompositeId = null;
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Get the FormTypeEntry so you can create an instance of it. Earlier, we used the entry stashed away in a holder.
    	// Here's another way to construct a FormTypeEntry for an existing form (the one we just created above).

    	// Find the application we created by id to ensure we have the unique app. Find the form we just created in this application.
    	String appId = request.getParameter("appId");
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		if (appEntry.getId().equals(appId)) {
    			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formtypeCompositeId = ftEntry.getId();
    				break;
    			}
    		}
    	}

    	
    	// Get the formType entry.
    	URL formTypeURL = fs.getEntryURL(FormTypeEntry.class, formtypeCompositeId);
    	assert (formTypeURL != null);
    	final FormTypeEntry ftEntry = (FormTypeEntry) fs.getEntry(formTypeURL, FormTypeEntry.class);
    	assert (ftEntry != null);    
    
    	// Params (optional). There are many parameters - let's set a few.
    	Map<String, Object> params = new HashMap<String, Object>(1);
    	// If set, any form action will be ignored and the document set (including attachments) will be returned when the form is submitted.
    	// Otherwise, normal form action processing is performed (default). We set it so we can retrieve the data later (see below).
    	params.put(FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER, "xml");
    	// Control form resizing. You can set this to
    	// FormTypeEntry.VALUE_RESIZE_TRUE  to turn auto-resizing on.
    	// FormTypeEntry.VALUE_RESIZE_HORIZONTAL to turn auto-resizing on horizontally only.
    	// FormTypeEntry.VALUE_RESIZE_VERTICAL  to turn auto-resizing on vertically only.
    	// Set to FormTypeEntry.VALUE_RESIZE_FALSE
    	params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, FormTypeEntry.VALUE_RESIZE_VERTICAL);
    	// Set the initial width (defaults to the width set in the form or 600px otherwise) for the iframe.
    	params.put(FormTypeEntry.EMBED_WIDTH_PARAMETER, "99%");
    	// Set FormTypeEntry.EMBED_SCROLLING_PARAMETER
    	// to "auto" (default) if you want to automatically show scrollbars if required.
    	// to "no" if you don't want scrollbars.
    	// to "yes" if you always want scrollbars.
    	params.put(FormTypeEntry.EMBED_SCROLLING_PARAMETER, "no");    	
    
    	// Create a new instance of the form, construct its entry and stash it away.
    	FormEntry fEntry = ftEntry.createInstance(params, null);
    	assert (fEntry != null);

    	instanceHolder = new EntryHolder (fEntry);  // this controller has session scope
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("formUrl", fEntry.getFormUseEmbedLink().getHref());
        return "test-form";
    	
    }
    
    @RequestMapping(value = "/testFormWithData", method = RequestMethod.GET)
    public String testFormWithDataForm(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("appId", request.getParameter("appId"));
        return "use-form-with-data";
    }
    
    @RequestMapping(value = "/testFormWithData", method = RequestMethod.POST)
    public String testFormWithData(Model model, HttpServletRequest request, @RequestParam("formDataFile") MultipartFile file) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	
    	if (file.isEmpty()) {
            model.addAttribute("formName", request.getParameter("formName"));
            model.addAttribute("appId", request.getParameter("appId"));
            model.addAttribute("errorMessage", "A form data XML file must be chosen");
            return "use-form-with-data";    		
    	}
    	
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	String formtypeCompositeId = null;
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by id to ensure we have the unique app. Find the form we just created in this application.
    	String appId = request.getParameter("appId");
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		if (appEntry.getId().equals(appId)) {
    			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formtypeCompositeId = ftEntry.getId();
    				break;
    			}
    		}
    	}

    	// Get the formType entry.
    	URL formTypeURL = fs.getEntryURL(FormTypeEntry.class, formtypeCompositeId);
    	assert (formTypeURL != null);
    	final FormTypeEntry ftEntry = (FormTypeEntry) fs.getEntry(formTypeURL, FormTypeEntry.class);
    	assert (ftEntry != null);    
    
    	// Params (optional). There are many parameters - let's set a few.
    	Map<String, Object> params = new HashMap<String, Object>(1);
    	// If set, any form action will be ignored and the document set (including attachments) will be returned when the form is submitted.
    	// Otherwise, normal form action processing is performed (default). We set it so we can retrieve the data later (see below).
    	params.put(FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER, "xml");
    	// Control form resizing. You can set this to
    	// FormTypeEntry.VALUE_RESIZE_TRUE  to turn auto-resizing on.
    	// FormTypeEntry.VALUE_RESIZE_HORIZONTAL to turn auto-resizing on horizontally only.
    	// FormTypeEntry.VALUE_RESIZE_VERTICAL  to turn auto-resizing on vertically only.
    	// Set to FormTypeEntry.VALUE_RESIZE_FALSE
    	params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, FormTypeEntry.VALUE_RESIZE_VERTICAL);
    	// Set the initial width (defaults to the width set in the form or 600px otherwise) for the iframe.
    	params.put(FormTypeEntry.EMBED_WIDTH_PARAMETER, "99%");
    	// Set FormTypeEntry.EMBED_SCROLLING_PARAMETER
    	// to "auto" (default) if you want to automatically show scrollbars if required.
    	// to "no" if you don't want scrollbars.
    	// to "yes" if you always want scrollbars.
    	params.put(FormTypeEntry.EMBED_SCROLLING_PARAMETER, "no");    	
    	
    	// Get the init document.
    	List<MediaSource> mss = new ArrayList<MediaSource>();
    	InputStream fis = new ByteArrayInputStream(file.getBytes()); // The XML document.
    	MediaStreamSource ms = new MediaStreamSource(fis, "application/xml");
    	ms.setName("Document");
    	mss.add(ms);
    	// You can add as many initial documents as you want to the ArrayList.    	
    	
    	// Create a new instance of the form, construct its entry and stash it away.
    	FormEntry fEntry = ftEntry.createInstance(params, mss);
    	assert (fEntry != null);

    	instanceHolder = new EntryHolder (fEntry);  // this controller has session scope
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("formUrl", fEntry.getFormUseEmbedLink().getHref());
        return "test-form";
    }
    
    /**
     * Entry point for submitting or canceling a use-mode (runtime) form.
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/submitUseModeForm", method = RequestMethod.POST)
    public String submitForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	if (instanceHolder != null) {
	    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
	    	
	    	FormEntry fEntry = instanceHolder.getEntry(fs, FormEntry.class);
	    	fEntry = fEntry.getSelf();

	    	String actionType = (String)request.getParameter("actionType");
        	if ("cancel".equals(actionType)) {
        		fEntry.cancelInstance();
        		return "redirect:home";
        	}
        	else if ("submit".equals(actionType)) {
		    	FormData data = fEntry.submitInstance(request.getHeader("Authorization"), null);
		    	if (data != null) {
		    		
		    		// Save the form data in session so that it can be downloaded later if needed
		    		request.getSession().setAttribute(LAST_FORM_DATA, data);
		    		
		    		List<Map<String,Object>> mapData = new ArrayList<Map<String,Object>>();
					for (Part part : data) {
						Map<String,Object> item = new HashMap<String,Object>();
						item.put("contentType", part.getContentType());
						String[] headers = part.getHeader("Content-Disposition");
						item.put("contentDisposition", (headers!=null&&headers.length>0?headers[0]:null));
						
						// The text/xml part is the form data payload.  This can be downloaded/saved to re-initialize the form again later.
						item.put("downloadable", false);
						if (part.getContentType().contains("text/xml")) {
							item.put("downloadable", true);
						}
						
						mapData.add(item);
						if (part.getContentType().contains("text/xml") || part.getContentType().contains("text/plain")) {
							item.put("data", new BufferedReader(new InputStreamReader(part.getInputStream(), Charset.forName("UTF-8")))
									  .lines().collect(Collectors.joining("\n")));
						}
					}
		    		
		    		
		            model.addAttribute("formName", request.getParameter("formName"));
		            model.addAttribute("formData", mapData);
		    		return "form-submission-data";
		    	}
        	}
    	}
    	return "redirect:home";
    }
    
    @RequestMapping(value = "/downloadFormData", method = RequestMethod.GET, produces = "text/xml")
    public @ResponseBody void downloadApp(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return;
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	FormData data = (FormData)request.getSession().getAttribute(LAST_FORM_DATA);
    	if (data == null)
    		return;
    	
		for (Part part : data) {
			// The text/xml part is the form data payload to download.
			if (part.getContentType().contains("text/xml")) {
		    	response.setContentType("text/xml; charset=utf-8");
				String[] headers = part.getHeader("Content-Disposition");
				response.setHeader("Content-Disposition", (headers!=null&&headers.length>0?headers[0]:null));
		        FileCopyUtils.copy(part.getInputStream(), response.getOutputStream());
				return;
			}
		}
		return;
    }
    
}
