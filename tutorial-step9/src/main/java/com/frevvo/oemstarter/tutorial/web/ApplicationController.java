package com.frevvo.oemstarter.tutorial.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frevvo.forms.client.ApplicationEntry;
import com.frevvo.forms.client.ApplicationFeed;
import com.frevvo.forms.client.FormTypeEntry;
import com.frevvo.forms.client.FormTypeFeed;
import com.frevvo.forms.client.FormsService;
import com.google.gdata.data.MediaContent;
import com.google.gdata.data.media.MediaSource;
import com.google.gdata.util.ResourceNotFoundException;

@Controller
public class ApplicationController {

    @RequestMapping(value = {"/downloadAppForm","/selectAppForm"}, method = RequestMethod.GET)
    public String downloadAppFormPage(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String lastFormName = request.getParameter("lastFormName");
    	if (lastFormName == null)
    		lastFormName = "";
        model.addAttribute("lastFormName", lastFormName);
        model.addAttribute("type", request.getParameter("type"));
        return "app-selection";
    }

    @RequestMapping(value = "/downloadApp", method = RequestMethod.GET, produces = "application/zip")
    public @ResponseBody void downloadApp(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return;
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by id to ensure we have the unique app. Find the form we just created in this application.
    	String formName = request.getParameter("formName");
    	String appId = request.getParameter("appId");
    	String appName = formName + EditFormController.APP_SUFFIX;
    	ApplicationEntry appEntry = null;
    	for (ApplicationEntry entry : feed.getEntries()) {
			if (entry.getId().equals(appId)) {
				appEntry = entry;
				break;
			}
    	}
    	
    	// download it
    	MediaContent mc = (MediaContent) appEntry.getContent();
    	assert (mc != null);
    	MediaSource ms = fs.getMedia(mc);
    	assert (ms != null);
    	assert ("application/zip".equals(ms.getContentType()));
    	assert (ms.getInputStream() != null);
    	
    	response.setContentType(ms.getContentType());
        response.setHeader("Content-Disposition", "attachment; filename=" + appName+".zip");
        FileCopyUtils.copy(ms.getInputStream(), response.getOutputStream());
    }
    
    @RequestMapping(value = "/deleteApp", method = RequestMethod.POST)
    public String deleteApp(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by id to ensure we have the unique app. Find the form we just created in this application.
    	String appId = request.getParameter("appId");
    	for (ApplicationEntry entry : feed.getEntries()) {
			if (entry.getId().equals(appId)) {
				entry.delete();
				try {
					entry = entry.getSelf();
					assert (false);
				} catch (ResourceNotFoundException e) {
					assert (true); // Exception is expected.
				}
				break;
			}
    	}    	
		return "redirect:home";
    }

    @RequestMapping(value = "/listApplications", method = RequestMethod.GET)
    public String listApps(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Get the app feed and display them.  For simplicity, always assuming that each
    	// application contains a single form for right now, so managing forms by managing the app.
    	List<Map<String, String>> apps = new ArrayList<Map<String, String>>();
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		Map<String, String> appInfo = new HashMap<String, String>();
    		apps.add(appInfo);
    		appInfo.put("app-name", appEntry.getTitle().getPlainText());
    		appInfo.put("app-id", appEntry.getId());
			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
				// Assuming there will be just one form.
	    		appInfo.put("form-name", ftEntry.getTitle().getPlainText());
	    		appInfo.put("form-id", ftEntry.getId());
				break;
			}
    	}
    	
        model.addAttribute("appList", apps);
		return "app-list";
    }
    
}
