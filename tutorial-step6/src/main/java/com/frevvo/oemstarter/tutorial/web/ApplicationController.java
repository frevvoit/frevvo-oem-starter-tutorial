package com.frevvo.oemstarter.tutorial.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.frevvo.forms.client.ApplicationEntry;
import com.frevvo.forms.client.ApplicationFeed;
import com.frevvo.forms.client.FormsService;
import com.google.gdata.data.MediaContent;
import com.google.gdata.data.media.MediaSource;

@Controller
public class ApplicationController {

    @RequestMapping(value = "/downloadAppForm", method = RequestMethod.GET)
    public String downloadAppFormPage(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String lastFormName = request.getParameter("lastFormName");
    	if (lastFormName == null)
    		lastFormName = "";
        model.addAttribute("lastFormName", lastFormName);
        return "app-selection";
    }

    @RequestMapping(value = "/downloadApp", method = RequestMethod.GET, produces = "application/zip")
    public @ResponseBody void downloadApp(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return;
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by name (you can also find it by ID). Find the form we just created in this application.
    	String formName = request.getParameter("formName");
    	String appName = request.getParameter("formName") + EditFormController.APP_SUFFIX;
    	ApplicationEntry appEntry = null;
    	for (ApplicationEntry entry : feed.getEntries()) {
			if (entry.getTitle().getPlainText().contains(appName)) {
				appEntry = entry;
				break;
			}
    	}
    	
    	// download it
    	MediaContent mc = (MediaContent) appEntry.getContent();
    	assert (mc != null);
    	MediaSource ms = fs.getMedia(mc);
    	assert (ms != null);
    	assert ("application/zip".equals(ms.getContentType()));
    	assert (ms.getInputStream() != null);
    	
    	response.setContentType(ms.getContentType());
        response.setHeader("Content-Disposition", "attachment; filename=" + appName+".zip");
        FileCopyUtils.copy(ms.getInputStream(), response.getOutputStream());
    }
}
