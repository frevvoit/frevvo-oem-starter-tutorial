package com.frevvo.oemstarter.tutorial.web;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frevvo.forms.client.FormsService;
import com.frevvo.forms.client.util.AutoLoginUserInfo;
import com.frevvo.oemstarter.tutorial.model.LoginInfo;
import com.google.gdata.util.ServiceException;

@Controller
@Scope("session")
public class MainController {

    @Value("${frevvo.default.protocol}")
    String defaultProtocol;
    @Value("${frevvo.default.port}")
    String defaultPort;
    @Value("${frevvo.default.hostName}")
    String defaultHostName;
    @Value("${frevvo.default.userId}")
    String defaultUserId;
    @Value("${frevvo.default.tenantAdminUserId}")
    String defaultTenantAdminUserId;
    @Value("${frevvo.default.tenantAdminPassword}")
    String defaultTenantAdminPassword;
    
    
    FormsService fs;
    private String loginAsUser;

    @RequestMapping(value= {"/", "/home"})
    public String homePage(Model model) {
    	if (this.fs == null) 
    		return "redirect:login";
        model.addAttribute("loginUser", this.fs.getUsername());
        model.addAttribute("loginAsUser", this.loginAsUser);
        return "main";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
    	if (this.fs == null) {
	        model.addAttribute("protocol", defaultProtocol);
	        model.addAttribute("port", defaultPort);
	        model.addAttribute("hostName", defaultHostName);
	        model.addAttribute("userId", defaultUserId);
	        model.addAttribute("tenantAdminUserId", defaultTenantAdminUserId);
	        model.addAttribute("tenantAdminPassword", defaultTenantAdminPassword);
	        return "login";
    	}
    	else {
	        model.addAttribute("loginUser", this.fs.getUsername());
	        model.addAttribute("loginAsUser", this.loginAsUser);
	        return "main";
    	}
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("SpringWeb")LoginInfo loginInfo, Model model) {
    	if (this.fs != null) {
	        model.addAttribute("loginUser", this.fs.getUsername());
	        model.addAttribute("loginAsUser", this.loginAsUser);
	        return "main";
    	}
    	// Else loginAs now
		try {
			this.fs = new FormsService(loginInfo.getProtocol(), loginInfo.getHostName(), loginInfo.getPort(), null);
		} catch (MalformedURLException e1) {
			// Failed
			this.initializeLoginInfoAttributes(loginInfo, model);
			return "login";  // re-display
		}
    	Map<String, String> customParams = new HashMap<String, String>(1);
    	customParams.put("autoLogin", "true");

    	// If you want to login as a designer user (who can create forms), pass in this role. Otherwise, pass in null.
    	List<String> roles = Collections.singletonList("frevvo.Designer");

    	AutoLoginUserInfo alui = null;
		try {
			alui = fs.loginAs(loginInfo.getUserId(), loginInfo.getTenantAdminUserId(), loginInfo.getTenantAdminPassword(), 
					          true, roles, loginInfo.getFirstName(), loginInfo.getLastName(), 
					          loginInfo.getEmailAddress(), customParams);
		} catch (ServiceException e) {
		}
    	if (alui == null) {
    		// LoginAs failed, re-display login
    		this.initializeLoginInfoAttributes(loginInfo, model);
	        return "login";
    	}
    	this.loginAsUser = alui.userId;
    	
    	// logged in
        return "redirect:home";
    }
    private void initializeLoginInfoAttributes(LoginInfo loginInfo, Model model) {
        model.addAttribute("protocol", loginInfo.getProtocol());
        model.addAttribute("port", loginInfo.getPort());
        model.addAttribute("hostName", loginInfo.getHostName());
        model.addAttribute("userId", loginInfo.getUserId());
        model.addAttribute("tenantAdminUserId", loginInfo.getTenantAdminUserId());
        model.addAttribute("tenantAdminPassword", loginInfo.getTenantAdminPassword());
        model.addAttribute("lastName", loginInfo.getLastName());
        model.addAttribute("firstName", loginInfo.getFirstName());
        model.addAttribute("emailAddress", loginInfo.getEmailAddress());
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(Model model) throws ServiceException {
    	if (this.fs != null) {
    		this.fs.logout();
    		this.fs = null;
    	}
        return "redirect:home";
    }
    
}