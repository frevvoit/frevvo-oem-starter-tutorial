# frevvo-oem-starter-tutorial

This starter tutorial is useful to partners wishing to integrate with frevvo Live Forms using the data api (Java).

This tutorial is intended for anyone wishing to integrate their web application with frevvo Live Forms. Frevvo Live Forms is a tool for designing and running forms and workflows.

More information on frevvo Live Forms can be obtained at [frevvo's site](http://www.frevvo.com) or in [frevvo's documentation](http://docs.frevvo.com).

## Knowledge Prerequisites

-   Basic knowledge of Java, HTML and web applications.
-   Basic knowledge of Spring, Sping MVC and Spring Boot.
-   Basic knowledge of Thymeleaf.
-   Some basic understanding of the frevvo Live Forms data api. This can be found [here](http://http://docs.frevvo.com/d/display/latest/Data+API).

## The Tutorial

In this tutorial we will build a spring boot web application that will use the frevvo data api to "surface" parts of the frevvo application useful to OEM partners. This web application is intended as a very simple stand-in for your application and illustrates the integrations useful with frevvo Live Forms. The parts of frevvo Live Forms most useful to OEM partners are the form designer and the form runtime. Other parts of frevvo Live Forms may also be exposed, but the designer and runtime are the most important and will be focused on here.

This Tutorial will walk you through step-by-step instructions to embed frevvo within your own web application. We'll describe one specific path for integration that's most commonly used by our customers. Obviously, there could be differences depending on the outer (embedding) application and its design. This Tutorial will also use the Java API – embedding with the .NET API is very similar.

The tutorial is organized as a series of development steps, each of which is included here as a complete Maven project. You may choose to either code up each step or simply review the code as you progress. Any of the step's web applications may be run from the command line as a spring boot application. As we proceed through each step, the relevant code changes will be pointed out, but if you are coding it yourself, care should be taken to examine the code carefully for minor changes.

```Shell
mvn compile
cd tutorial-step1
mvn clean
mvn spring-boot:run
```

The mvn clean is required the first time in order to properly install required provided dependencies into the local repository.

Each tutorial step's spring boot web app is configured in src/main/resources/application.properties:

```Properties
server.port=8085

spring.thymeleaf.cache=false
spring.thymeleaf.enabled=true
spring.thymeleaf.prefix=classpath:/templates/
spring.thymeleaf.suffix=.html

spring.application.name=OEM Starter Tutorial Step 1

frevvo.default.protocol=http
frevvo.default.port=8082
frevvo.default.hostName=localhost
frevvo.default.userId=designer
frevvo.default.tenantAdminUserId=admin@dsm
frevvo.default.tenantAdminPassword=admin
server.servlet.session.cookie.path=/oemapp
server.servlet.context-path=/oemapp
```

The `server.port` configures the tutorial web application's port. `frevvo.default.port`, `frevvo.default.hostName`, etc. configure the specifics of the frevvo Live Forms instance that the application is integrating with so that the tutorial app can reach frevvo Live Forms.

Based on the above config, the running spring boot web application for the tutorial step can be accessed in the browser at: http://localhost:8085/oemapp.

### Dependencies

The tutorial is built using the frevvo data api. There are a number of libraries required that are documented in the [frevvo docs for the data api](http://docs.frevvo.com/d/display/frevvo74/Data+API). The frevvo data api client library jar file is included in the tutorial (/lib/forms-java-X.Y.jar). It is especially important to use the jar file version corresponding to the version of frevvo that you are integrating with.

### Tutorial Prerequisites

#### Create a tenant with Delegating Security Manager

You'll need an in-house installation of frevvo. If you prefer to use a cloud account, you'll need to contact frevvo. After you have completed the in-house installation, you'll need to create a tenant. For this tutorial, we'll use the the Delegating Security Manager. This security manager is explicitly designed for external applications that want to embed frevvo. Users and roles are defined outside frevvo (in your application) and your application is responsible for authentication and for passing credentials to frevvo.

Create a tenant for this integration:

1. Browse this URL - http://localhost:8082/frevvo/web/login - Change the <localhost:8082> to the server:port where you installed Live Forms, if necessary
1. Login to Live Forms as user admin@d, password "admin".
1. Click the "Manage Tenants" link on the page that displays.
1. There is a single tenant named d. This is the default tenant.
1. Click the ![plus](http://docs.frevvo.com/d/download/attachments/19805182/add.gif?version=1&modificationDate=1509723774000&api=v2 'plus') icon to create a tenant and fill in the form. In the Security Manager Class pick list, choose Delegating Security Manager. Your new tenant will be created.
   For the purposes of this tutorial, we'll assume you've created a tenant called dsm with Delegating Security Manager set, the admin user id is "admin" with password "admin". Of course, you should pick a more secure password.

We'll also assume that your tutorial application (for any step) is accessed using a URL like http://localhost:/oemapp (port configured as discussed above).

Conceptually, the integration described here looks like the image below.

![plus](http://docs.frevvo.com/d/download/attachments/21533567/integration-1.png?version=1&modificationDate=1535559207000&api=v2 'plus')

#### Some basic concepts

frevvo is organized as follows:

-   Tenants have users: designers and non-designers.
-   Designers can create and edit forms and flows.
-   Non-designers can use/administer them, view submissions etc. but cannot create or edit them.
-   A designer user organizes forms and flows in Applications. You can have any number of Applications in a user. The Application is the basic downloadable unit in frevvo. While you can download individual forms and flows, we recommend sticking to Applications since they are self-contained.
-   Each Application can hold an arbitrary number of schemas (XSDs), forms and flows. In frevvo, the form and flow designs are referred to as FormType and FlowType respectively. Individual instances are referred to as Form and Flow respectively. You'll notice that methods in the API use these terms e.g. FormTypeEntry refers to the FormType and FormEntry refers to an individual Form (an instance of the FormType).
-   A FormType or FlowType can refer to multiple DocumentTypes. Note: if you aren't planning to use XML Schemas, you can ignore this part. As you might expect, an individual Form (an instance of the FormType) will have one or more Documents (instances of the corresponding DocumentTypes).
    -   A form that's designed top-down – starting with a blank form and dragging and dropping controls into the canvas – will have a single DocumentType in it (sometimes referred to as the from-scratch DocumentType).
    -   A form that's designed bottom-up – from one or more XML Schemas (XSD) – will have multiple DocumentTypes. There will always be the from-scratch DocumentType since you can always drag/drop controls. In addition, there will be a DocumentType per XSD root element that you add to the form.

**For OEM integrations, we strongly recommend that you have a 1-1 relationship between an Application and a form or flow (technically a FormType or FlowType). Specifically, you should always create a new Application and in that Application have a single form or flow. You can then download and manage the frevvo Application easily in the context of your own system.**

### Tutorial Step 1: Create the Spring Boot App and Authenticate to frevvo from your application

Refer to the /tutorial-step1 sub-directory for all code discussed here and to run the spring boot web application.

You can either create a new Spring Boot app project image or you can start with the included tutorial-step1 project which already includes all code discussed in this section (recommended).

In order to automatically integrate with Live Forms, your web application will first need to establish a session using the Live Forms API. This is done by using the com.frevvo.forms.client.FormsService class and by providing proper credentials to authenticate.

In most cases, you will want to use the loginAs() method. loginAs() allows you to login to frevvo as any tenant user provided you pass in the tenant admin's user and password credentials. This is convenient when you want login to frevvo using the same user that is logged into your application without having to know their password.

![LoginAs to Frevvo](http://docs.frevvo.com/d/download/attachments/21533567/loginAs.png?version=2&modificationDate=1535554080000&api=v2&effects=border-simple,blur-border 'LoginAs to Frevvo')

These are the relevant files in tutorial step 1.

| File                | Location                                            | Description                                                     |
| ------------------- | --------------------------------------------------- | --------------------------------------------------------------- |
| login.html          | source/main/resoures/templates                      | Login page to collect information for authenticating to frevvo. |
| main.html           | source/main/resoures/templates                      | Main age displayed after login                                  |
| MainController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for login to frevvo.                      |

The most relevant bit of MainController.java actually does the authentication to frevvo.

```Java
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("SpringWeb")LoginInfo loginInfo, Model model) {
    	if (this.fs != null) {
	        model.addAttribute("loginUser", this.fs.getUsername());
	        model.addAttribute("loginAsUser", this.loginAsUser);
	        return "main";
    	}
    	// Else loginAs now
	try {
		this.fs = new FormsService(loginInfo.getProtocol(), loginInfo.getHostName(), loginInfo.getPort(), null);
	} catch (MalformedURLException e1) {
		// Failed
		this.initializeLoginInfoAttributes(loginInfo, model);
		return "login";  // re-display
	}
    	Map<String, String> customParams = new HashMap<String, String>(1);
    	customParams.put("autoLogin", "true");

    	// If you want to login as a designer user (who can create forms), pass in this role. Otherwise, pass in null.
    	List<String> roles = Collections.singletonList("frevvo.Designer");

    	AutoLoginUserInfo alui = null;
	try {
		alui = fs.loginAs(loginInfo.getUserId(), loginInfo.getTenantAdminUserId(), loginInfo.getTenantAdminPassword(),
					  true, roles, loginInfo.getFirstName(), loginInfo.getLastName(),
					  loginInfo.getEmailAddress(), customParams);
	} catch (ServiceException e) {
	}
    	if (alui == null) {
    		// LoginAs failed, re-display login
    		this.initializeLoginInfoAttributes(loginInfo, model);
	        return "login";
    	}
    	this.loginAsUser = alui.userId;

    	// logged in
        return "redirect:home";
    }

```

FormService constructor parameters:

-   Tprotocol is one of http or https
-   host is the frevvo server host name
-   port is the port on which frevvo is running

FormService.loginAs parameters:

-   userId is the id of the user you wish to login as.
-   tenantAdminUserId is the user id of the tenant admin user specified using the syntax adminUserId@tenantName
-   tenantAdminPassword is the password of the tenant admin user
-   virtual: if set to true, does not actually create the user. For now, leave it set to false so the user is actually created.
-   roles: You can provide a list of roles. For now, set the role frevvo.Designer. This creates the user and sets permissions so he/she can design forms. Of course, you can pass a list with as many roles as you want. You can also pass in null if you do not want to specify any roles.
-   customParams: For now, set the autoLogin parameter with value true.

If you start the tutorial-step1 web app (mvn spring-boot:run) and open a browser tab to http://localhost:8085/oemapp, you will immediately see the "Please Enter Frevvo Login Information" screen. Users may be created "on the fly", in which case, the user's first and last name are needed as well as the email.

Since we chose the Delegating Security Manager, the user will be automatically created if the specified userId does not exist. Now, you are logged in from your application to frevvo as a designer user.

### Tutorial Step 2: Create a new blank form for editing and render it

Refer to the /tutorial-step2 sub-directory for all code discussed here and to run the spring boot web application.

Now that we're logged in as a designer user, let's create a new form and display the frevvo designer. Our goal is to render the frevvo designer embedded inside your web application in the browser. The end user accesses your application page (e.g. http://localhost:8085/oemapp/createAndEditForm). The resulting page has your outer page content + the embedded frevvo form designer.

![Create Form Frevvo](http://docs.frevvo.com/d/download/attachments/21533567/embeddedDesigner2.png?version=3&modificationDate=1535570774000&api=v2&effects=border-simple,blur-border 'Create Form Frevvo')

These are the relevant files in tutorial step 2.

| File                    | Location                                            | Description                                                                                                                                         |
| ----------------------- | --------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| new-form.html           | source/main/resoures/templates                      | HTML Form for collecting the new form's intended name.                                                                                              |
| edit-form.html          | source/main/resoures/templates                      | HTML page that displays the newly created form in the fevvo form designer within an iframe. The form designer is being served by frevvo Live Forms. |
| EditFormController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for controlling the editing of forms from frevvo.                                                                             |

The most relevant bit of EditFormController.java actually creates the new form type in frevvo and an editing instance that will be shown in the iframe.

```Java
    @RequestMapping(value = "/createAndEditForm", method = RequestMethod.POST)
    public String editForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	String formName = request.getParameter("formName");
    	if (formName==null || formName.length()==0)
    		return "redirect:newForm";

    	// Now create the app/form
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationEntry appEntry = new ApplicationEntry();
    	// Using form name for both the applicaton name and form name in frevvo
    	appEntry.setTitle(new PlainTextConstruct(formName + " (App)"));
    	// Insert it on Frevvo
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);
    	appEntry = feed.insert(appEntry);

    	// Create form
    	FormTypeEntry ftEntry = new FormTypeEntry();
    	ftEntry.setTitle(new PlainTextConstruct(formName));
    	ftEntry.setVisibility(Visibility.PUBLICTENANT);
    	// Insert it into Frevvo application
    	ftEntry = appEntry.getFormTypeFeed().insert(ftEntry);
    	assert (ftEntry != null);

    	FormEntry fEntry = this.createEditingInstance(ftEntry, null, request, fs);
    	assert (fEntry != null);
    	Map<String, Object> params = new HashMap<String, Object>(4);
	    // You can add any URL params here.
	    params.put(FormTypeEntry.EMBED_EXT_SUBMIT_PARAMETER, true);
	    params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, true);
	    String iframeId = "12345";
	    params.put("_iframe_id", iframeId);
	    params.put("_referrer_url", "blah");
    	String hRef = fEntry.getFormEditLink(params).getHref();

    	// Save the type and instance in this controller which has session scope
    	this.typeHolder = new EntryHolder(ftEntry);
    	this.instanceHolder = new EntryHolder (fEntry);

        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";
    }

    protected FormEntry createEditingInstance(FormTypeEntry myEntry,
			Map<String, Object> params, HttpServletRequest req, FormsService formsService)
			throws ServiceException, MalformedURLException, IOException {
	    if (params == null)
		    params = new HashMap<String, Object>();
	    params.put("edit", true);

	    URL fUrl = myEntry.createFormInstance(params, null,
			req.getHeader(AUTHORIZATION_HEADER_NAME), null);
	    assert (fUrl != null);

	    // ATTEMPT BASED ON PATTERNS IN FormTypeEntry.java
	    String entryId = Helper.getFormEntryIdFromInstance(fUrl);
	    URL entryUrl = formsService.getEntryURL(FormEntry.class, entryId);
	    return formsService.getEntry(entryUrl, FormEntry.class);
   }
```

The code is somewhat self-explanatory. We first create a new Application in the designer user, then we create a new FormType and insert it in the Application, and finally we create an editing instance of the FormType. Now, we have a FormEntry that represents the editing form instance on the frevvo server and a link (URL) to that editing form instance. You should save the FormTypeEntry and FormEntry in your server-side code since you'll need them later. frevvo's API provides a serializable class for this purpose that you'll need to use called EntryHolder.

Lastly, render the form designer embedded in an iframe in edit-form.html.

```HTML
<html>
<head>
	<title>Edit Form Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2 th:text="'Editing Form: ' + ${formName}">Editing Form: Unknown</h2>

	<iframe width=1280 height=800 th:id="${iframeId}" th:src="${iframeSrc}">
</body>
</html>
```

This will render your web page with the internal iframe coming from the frevvo server. The internal iframe will display the frevvo form designer embedded in your page.

### Tutorial Step 3: Save, Finish, and Cancel

Refer to the /tutorial-step3 sub-directory for all code discussed here and to run the spring boot web application.

It's also a very common requirement to hide the frevvo designer toolbar buttons and replace them with buttons or menu items consistent with your existing web page design to Save, Finish, and Cancel the form. Let's take a look at these – they are all very similar. First, add the buttons to edit-form.html. Clicking each button simply links to a URL in the tutorial application.

```HTML
<html>
<head>
	<title>Edit Form Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2 th:text="'Editing Form: ' + ${formName}">Editing Form: Unknown</h2>

 	<form id="submitForm" method = "POST" action = "submitForm">
 		<input name="actionType" type="hidden"/>
 		<input type="submit" value="Save Form" onclick="document.querySelector('input[name=actionType]').value='save';document.querySelector('#submitForm').submit();" />
 		<input type="submit" value="Finish and Save Form" onclick="document.querySelector('input[name=actionType]').value='finish';document.querySelector('#submitForm').submit();" />
 		<input type="submit" value="Cancel Form" onclick="document.querySelector('input[name=actionType]').value='cancel';document.querySelector('#submitForm').submit();" />
  	</form>
	<iframe width=1280 height=800 th:id="${iframeId}" th:src="${iframeSrc}">

</body>
</html>
```

The net change is three submit buttons appearing above the iframe containing the form editor. These buttons post respectively to /save, /finish and /cancel and allow the outer OEM application to control saving, finishing and canceling the form edit.

The posts are handled in EditFormController:

```Java
    @RequestMapping(value = "/submitForm", method = RequestMethod.POST)
    public String submitForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String actionType = (String)request.getParameter("actionType");

    	if ("save".equals(actionType))
    		return saveEditingForm(model, request);
    	else if ("finish".equals(actionType))
    		return saveAndFinishEditingForm(request);
    	else if ("cancel".equals(actionType))
    		return cancelEditingForm(request);

    	return "redirect:home";   // should not get here
    }

    private String saveEditingForm(Model model, HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(true, request.getHeader(AUTHORIZATION_HEADER_NAME), null);

    	// Saved earlier in typeHolder
    	FormTypeEntry ftEntry = this.typeHolder.getEntry(fs, FormTypeEntry.class);
    	assert (ftEntry != null);
    	fEntry = createEditingInstance(ftEntry, null, request, fs);
    	assert (fEntry != null);
    	String hRef = getEditLinkUrl(fEntry);

    	// Stash it away.
    	EntryHolder instanceHolder = new EntryHolder (fEntry);

    	// re-render the editing form
        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";
    }

    private String saveAndFinishEditingForm(HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(true, request.getHeader(AUTHORIZATION_HEADER_NAME), null);
    	return "redirect:home";
    }

    private String cancelEditingForm(HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(false, request.getHeader(AUTHORIZATION_HEADER_NAME), null);
    	return "redirect:home";
    }

```

The save button is the most complicated because you will have to refresh the page – under the covers, frevvo has saved and committed the editing form, cleaned up, created a brand new instance with a different ID and URL. The old instance is no longer valid so we have to refresh the page using the URL for the newly created instance.

The (save and) finish button/operation is identical to the Save button except you don't create and re-render a new instance. The browser will now display a page rendered by your web application after the form is finished.

The cancel button/operation is identical to the Finish button except for the false instead of true parameter on the last line.

### Tutorial Step 4: Test the form you just designed.

Refer to the /tutorial-step4 sub-directory for all code discussed here and to run the spring boot web application.

These are the relevant files in tutorial step 4.

| File                       | Location                                            | Description                                                                                 |
| -------------------------- | --------------------------------------------------- | ------------------------------------------------------------------------------------------- |
| test-form-selection.html   | source/main/resoures/templates                      | HTML Form for collecting the name of the form to be tested/run.                             |
| test-form.html             | source/main/resoures/templates                      | HTML page that displays the runtime instance of the form being served by frevvo Live Forms. |
| UseModeFormController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for controlling the runtime use of forms from frevvo.                 |

A "Test Form" button has been added in main.html which you will see upon authentication to frevvo. This button results in a form page to input the form name of a previously designed and finished form that is to be tested.

UseModeFormController handles instantiating the use mode form for testing and forwarding to the test-form.html page for display of the use mode (runtime) form. The relevant code in UseModeFormController is:

```Java
    @RequestMapping(value = "/testForm", method = RequestMethod.POST)
    public String testForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	String formtypeCompositeId = null;
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Get the FormTypeEntry so you can create an instance of it. Earlier, we used the entry stashed away in a holder.
    	// Here's another way to construct a FormTypeEntry for an existing form (the one we just created above).

    	// Find the application we created by name (you can also find it by ID). Find the form we just created in this application.
    	String appName = request.getParameter("formName") + EditFormController.APP_SUFFIX;
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		if (appEntry.getTitle().getPlainText().contains(appName)) {
    			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formtypeCompositeId = ftEntry.getId();
    				break;
    			}
    		}
    	}


    	// Get the formType entry.
    	URL formTypeURL = fs.getEntryURL(FormTypeEntry.class, formtypeCompositeId);
    	assert (formTypeURL != null);
    	final FormTypeEntry ftEntry = (FormTypeEntry) fs.getEntry(formTypeURL, FormTypeEntry.class);
    	assert (ftEntry != null);

    	// Params (optional). There are many parameters - let's set a few.
    	Map<String, Object> params = new HashMap<String, Object>(1);
    	// If set, any form action will be ignored and the document set (including attachments) will be returned when the form is submitted.
    	// Otherwise, normal form action processing is performed (default). We set it so we can retrieve the data later (see below).
    	params.put(FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER, "xml");
    	// Control form resizing. You can set this to
    	// FormTypeEntry.VALUE_RESIZE_TRUE  to turn auto-resizing on.
    	// FormTypeEntry.VALUE_RESIZE_HORIZONTAL to turn auto-resizing on horizontally only.
    	// FormTypeEntry.VALUE_RESIZE_VERTICAL  to turn auto-resizing on vertically only.
    	// Set to FormTypeEntry.VALUE_RESIZE_FALSE
    	params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, FormTypeEntry.VALUE_RESIZE_VERTICAL);
    	// Set the initial width (defaults to the width set in the form or 600px otherwise) for the iframe.
    	params.put(FormTypeEntry.EMBED_WIDTH_PARAMETER, "99%");
    	// Set FormTypeEntry.EMBED_SCROLLING_PARAMETER
    	// to "auto" (default) if you want to automatically show scrollbars if required.
    	// to "no" if you don't want scrollbars.
    	// to "yes" if you always want scrollbars.
    	params.put(FormTypeEntry.EMBED_SCROLLING_PARAMETER, "no");

    	// Create a new instance of the form, construct its entry and stash it away.
    	URL fUrl = ftEntry.createFormInstance(params, null, null, null);
    	assert (fUrl != null);
    	String formId = FormsService.SCHEME_KIND_FLOW.equals(ftEntry.getKind()) ? Helper.extractFlowId(fUrl) : Helper.extractFormId(fUrl);
    	assert (formId != null);

    	FormEntry fEntry = null;
    	FormFeed fFeed = ftEntry.getFormFeed();
    	assert (fFeed != null);
    	for (FormEntry fE : fFeed.getEntries()) {
    		if (fE.getId().startsWith(formId)) {
    			fEntry = fE;
    			break;
    		}
    	}
    	assert (fEntry != null);

    	instanceHolder = new EntryHolder (fEntry);  // this controller has session scope
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("formUrl", fEntry.getFormUseEmbedLink().getHref());
        return "test-form";

    }
```

We find the formType, retrieve its FormTypeEntry, create a new instance of the form, retrieve its FormEntry and save it. Lastly, we display the form embedded in your application's web page using an iframe. This is in test-form.html.

```HTML
<html>
<head>
	<title>Test Form Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2 th:text="'Testing Form: ' + ${formName}">Testing Form: Unknown</h2>

	<div id="wrapper">
		<script th:src="${formUrl}" type="text/javascript"></script>
	</div>

</body>
</html>
```

The important part is the <script> tag as shown above that uses the form url gathered from frevvo in UseModeFormController. That's it. frevvo does the rest – the form instance will appear embedded in the browser.

### Tutorial Step 4a: Design and test a form that invokes an OAuth secured service

Refer to the /tutorial-step4a sub-directory for all code discussed here and to run the spring boot web application.

These are the relevant files in tutorial step 4a.

| File                 | Location                                            | Description                                                                                      |
| -------------------- | --------------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| login.html           | source/main/resoures/templates                      | Login page to collect information for authenticating to frevvo and authorization to the service. |
| MainController.java  | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for login to frevvo and providing service OAuth credentials.               |
| OAuthTestApp_app.zip | source/test/resources                               | Sample form to list contents from a google drive account                                         |

The most relevant bit of MainController.java actually does the authentication to frevvo and provides service OAuth credentials.

```Java
		try {
			alui = fs.loginAs(loginInfo.getUserId(), loginInfo.getTenantAdminUserId(), loginInfo.getTenantAdminPassword(),
					          true, roles, loginInfo.getFirstName(), loginInfo.getLastName(),
					          loginInfo.getEmailAddress(), customParams, loginInfo.getOauthCredential());
		} catch (ServiceException e) {
		}
```

FormService.loginAs additional parameters to step 1:

-   OAuth credential: This is made of the following sub-parameters:
    -   Service Host: The service URL host name
    -   Service Port: The Service URL port,if any
    -   Service Scheme: The URL scheme, typically http or https
    -   Client ID: The OAuth OEM application registered client ID
    -   Client Secret: The OAuth application registered client secret
    -   Access Token URI: The Service URI to get an access token
    -   Refresh Token: A valid long-lived service refresh token (optional, but if provided will get a new access token in the event the supplied access token expires)
    -   Access Token: The unexpired service access token (optional, if refresh token is provided)
    -   Access Token Expiration: When the access token will expire (if not provided, the token is assumed to be valid forever)

While you can test with the OAuth service of your choice, google drive is a easily accessible service. The login page defaults the service to google drive.
Please refer to the frevvo documentation for instructions on how to register for the google drive service and generate a refresh token by installing the google connector.
An application with a pre-designed form (OAuthTestApp_app.zip) that access the google drive and list the contents in a table is provided. You can upload this application to the designer user by logging in as admin@dsm in frevvo and then logging in as designer.
This form uses google drive API directly (does not use the google connector), so you need to remove the OAuth2 prefix from the refresh token generated by the connector.

-   Google Drive OAuth refresh/Access Token Example:
    -   Access google auth2 playgound URL : https://developers.google.com/oauthplayground/
    -   On Step1 (Select & authorize APIs) select all the options for Google Docs API v1 and click on Authorize APIs button <img src="./GoogleAuth2PlaygroundGoogleDocsAPI.png" />
    -   On Step2 (Exchange authorization code for tokens) click the "Exchange authorization code for tokens" button to generate refresh and access token, copy the refresh and access token <img src="./GoogleAuth2PlaygroundGoogleDocsAPIRefreshToken.png" /> <img src="./UseGoogleDocAPIRefreshtoken.png" />

### Tutorial Step 5: Submit the form and retrieve submission data

Refer to the /tutorial-step5 sub-directory for all code discussed here and to run the spring boot web application.

These are the relevant files in tutorial step 5.

| File                      | Location                       | Description                                                                            |
| ------------------------- | ------------------------------ | -------------------------------------------------------------------------------------- |
| form-submission-data.html | source/main/resoures/templates | HTML page to display the form submission data returned by frevvo upon form submission. |

A typical requirement is for the outer OEM application to control the submit or cancel to the runtime form instance (and for the corresponding buttons on the form itself to be hidden). To that end, we will add these buttons to test-form.html.

```HTML
<html>
<head>
	<title>Test Form Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2 th:text="'Testing Form: ' + ${formName}">Testing Form: Unknown</h2>

 	<form id="submitForm" method = "POST" action = "submitUseModeForm">
 		<input name="actionType" type="hidden"/>
 		<input name="formName" type="hidden" th:value="${formName}"/>
 		<input class="button" type="submit" value="Cancel Form" onclick="document.querySelector('input[name=actionType]').value='cancel';document.querySelector('#submitForm').submit();" />
 		<input class="button" type="submit" value="Submit Form" onclick="document.querySelector('input[name=actionType]').value='submit';document.querySelector('#submitForm').submit();" />
  	</form>
	<div id="wrapper">
		<script th:src="${formUrl}" type="text/javascript"></script>
	</div>

</body>
</html>
```

The net change is two submit buttons appearing above the embedded runtime form (script tag). These buttons post to /submitUseModeForm with different actionTypes of cancel and save.

The posts are handled in UseModeFormController:

```Java
    @RequestMapping(value = "/submitUseModeForm", method = RequestMethod.POST)
    public String submitForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	if (instanceHolder != null) {
	    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

	    	FormEntry fEntry = instanceHolder.getEntry(fs, FormEntry.class);
	    	fEntry = fEntry.getSelf();

	    	String actionType = (String)request.getParameter("actionType");
        	if ("cancel".equals(actionType)) {
        		fEntry.cancelInstance();
        		return "redirect:home";
        	}
        	else if ("submit".equals(actionType)) {
		    	FormData data = fEntry.submitInstance(request.getHeader("Authorization"), null);
		    	if (data != null) {
		    		List<Map<String,Object>> mapData = new ArrayList<Map<String,Object>>();
					for (Part part : data) {
						Map<String,Object> item = new HashMap<String,Object>();
						item.put("contentType", part.getContentType());
						String[] headers = part.getHeader("Content-Disposition");
						item.put("contentDisposition", (headers!=null&&headers.length>0?headers[0]:null));
						mapData.add(item);
						if (part.getContentType().contains("text/xml") || part.getContentType().contains("text/plain")) {
							item.put("data", new BufferedReader(new InputStreamReader(part.getInputStream(), Charset.forName("UTF-8")))
									  .lines().collect(Collectors.joining("\n")));
						}
					}


		            model.addAttribute("formName", request.getParameter("formName"));
		            model.addAttribute("formData", mapData);
		    		return "form-submission-data";
		    	}
        	}
    	}
    	return "redirect:home";
    }

```

This code programmatically submits the form and retrieves the data and documents that were sent if you set the FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER to a non-null value as shown in the sample code above. This is the recommended approach.

The parts of the submission data are displayed for now in form-submission-data.html. Experiment with different form designs.

### Tutorial Step 6: Download the Application

Refer to the /tutorial-step6 sub-directory for all code discussed here and to run the spring boot web application.

In most cases, OEM partners who embed frevvo's software prefer to save the form definitions (metadata) in their own systems. That enables them to fully control storage, access, permissions etc. frevvo's downloadable unit is the Application. If you remember, before we created a form for editing we first created an Application and added it to the designer user and then placed the form in this Application. You can download the Application and save it yourself.

These are the relevant files in tutorial step 6.

| File                       | Location                                            | Description                                                                                       |
| -------------------------- | --------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| app-selection.html         | source/main/resoures/templates                      | HTML Form for collecting the name of the application already existing in frevvo to be downloaded. |
| ApplicationController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for maniplulating applications in frevvo.                                   |

First a button is added to main.html for downloading.

```HTML
<a th:href="'downloadAppForm?lastFormName=' + ${lastFormName}" class="button">Download Application/Form</a>
```

The new app-selection.html page simply presents the form for application name selection (not shown).

ApplicationController handles downloading the application from frevvo. The relevant code in ApplicationController is:

```Java
    @RequestMapping(value = "/downloadApp", method = RequestMethod.GET, produces = "application/zip")
    public @ResponseBody void downloadApp(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return;
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by name (you can also find it by ID). Find the form we just created in this application.
    	String formName = request.getParameter("formName");
    	String appName = request.getParameter("formName") + EditFormController.APP_SUFFIX;
    	ApplicationEntry appEntry = null;
    	for (ApplicationEntry entry : feed.getEntries()) {
			if (entry.getTitle().getPlainText().contains(appName)) {
				appEntry = entry;
				break;
			}
    	}

    	// download it
    	MediaContent mc = (MediaContent) appEntry.getContent();
    	assert (mc != null);
    	MediaSource ms = fs.getMedia(mc);
    	assert (ms != null);
    	assert ("application/zip".equals(ms.getContentType()));
    	assert (ms.getInputStream() != null);

    	response.setContentType(ms.getContentType());
        response.setHeader("Content-Disposition", "attachment; filename=" + appName+".zip");
        FileCopyUtils.copy(ms.getInputStream(), response.getOutputStream());
    }
```

### Tutorial Step 7: Delete the Application

Refer to the /tutorial-step7 sub-directory for all code discussed here and to run the spring boot web application.

You can optionally delete the application from frevvo so that it is only saved in your own system. Of course, once deleted you cannot instantiate forms from this application. Many OEM partners will use one physical instance of the frevvo server for design and a separate physical instance for filling forms. If so, you can design forms, test them, download and delete from your design-time system and deploy them to your run-time system using your own deployment rules.

These are the relevant files in tutorial step 7.

| File                       | Location                                            | Description                                                                                                      |
| -------------------------- | --------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| app-selection.html         | source/main/resoures/templates                      | HTML Form for collecting the name of the application already existing in frevvo to be downloaded **or deleted**. |
| ApplicationController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for maniplulating applications in frevvo. **Handles downloading and deleting**.            |

First a button is added to main.html for deleting.

```HTML
<a th:href="'selectAppForm?type=delete&lastFormName=' + ${lastFormName}" class="button">Delete Application/Form</a>
```

Next, app-selection.html is modified a bit to handle both downloading and deleting.

```HTML
<html>
<head>
	<title>Select Application Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>Select Application/Form</h1>

	<!-- form with method GET.  Weird but works. -->
	<form id="submitForm" th:method = "((${type}=='download')? 'GET' : 'POST')" th:action = "${type}+App">
         <table>
            <tr>
               <td><label>Form Name</label></td>
               <!--  th:onclick="document.querySelector('#submitForm').setAttribute('action',${type}+'App']); document.querySelector('#submitForm').submit();" -->
               <td><input name="formName" th:value="${lastFormName}"/></td>
            </tr>
            <tr>
               <td></td>
               <td><input class="button" type="submit" th:value="${#strings.toUpperCase(type)}"/></td>
            </tr>
         </table>
      </form>
</body>
</html>
```

ApplicationController.java is now modified to additionally handle deleting the application from frevvo. The relevant code in ApplicationController is:

```Java
    @RequestMapping(value = "/deleteApp", method = RequestMethod.POST)
    public String deleteApp(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by name (you can also find it by ID). Find the form we just created in this application.
    	String appName = request.getParameter("formName") + EditFormController.APP_SUFFIX;
    	for (ApplicationEntry entry : feed.getEntries()) {
		if (entry.getTitle().getPlainText().contains(appName)) {
			entry.delete();
			try {
				entry = entry.getSelf();
				assert (false);
			} catch (ResourceNotFoundException e) {
				assert (true); // Exception is expected.
			}
			break;
		}
    	}
	return "redirect:home";
    }
```

### Tutorial Step 8: Design a form using an XML schema - Create a form for editing from a schema

Refer to the /tutorial-step8 sub-directory for all code discussed here and to run the spring boot web application.

This step is similar to step 2 in which a new form was created for editing, except here we will be creating the form from a schema. Now that we're logged in as a designer user, let's create a new form and display the frevvo designer. This time, we have an XML schema that we want to use as the metadata for the form's controls. As before, our goal is to render the frevvo designer embedded inside your web application in the browser.

First, the existing new-form.html is modified to allow and optional selection of a schema zip file when entering the new form's name. The schema can either be an internal built in system test schema that is included in the tutorial app or a file that the user selects. The built in system test schema is found in the source at /src/main/resources/systemSchema, but there is no need upload that file, just choose the default radio option. There are also fields to optionally choose a schema name and supply a root schema.

If you are uploading a zip file containing multiple related schemas (that import/include each other), please specify the exact name (relative to the top level directory separated by / (forward slash)) of the XSD file containing the root schema. A zip file cannot contain multiple unrelated schemas. You must upload each set of related schemas separately. For the built in system test schema, the root xsd file must be filled in as "inRootXFullExtension.xsd".

The modified new-form.html:

```HTML
<html>
<head>
	<title>Create New Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2>Create New Form</h2>

	<form method = "POST" action = "createAndEditForm" enctype="multipart/form-data">
         <table>
            <tr>
               <span class="error-message" colspan="2" th:text="${errorMessage}"></span>
            </tr>
            <tr>
               <td><label>Form Name</label></td>
               <td><input name="formName" th:value="${formName}"/></td>
            </tr>
            <tr>
               <td><label>From Schema</label></td>
               <td><input type="checkbox" name="fromSchema" th:checked="${fromSchema}"/></td>
            </tr>
            <tr>
               <td><label>Schema</label></td>
               <td><input type="radio" name="schemaSource" th:checked="(${schemaSource} == 'system')" value="system"> Use Builtin System Test Schema</td>
            </tr>
            <tr>
               <td><label> </label></td>
               <td><input type="radio" name="schemaSource" th:checked="(${schemaSource} == 'upload')" value="upload"> Upload Schema</td>
            </tr>
            <tr>
               <td><label>XSD Zip File</label></td>
               <td><input type="file" name="schemaFile"></td>
            </tr>
            <tr>
               <td><label>Schema Name</label></td>
               <td><input name="schemaName" th:value="${schemaName}"/></td>
            </tr>
            <tr>
               <td><label>Root XSD File</label></td>
               <td><input name="rootXsdFile" th:value="${rootXsdFile}"/></td>
            </tr>

            <tr>
               <td></td>
               <td><input class="button" type="submit" value="Create and Edit Form"/></td>
            </tr>
         </table>
      </form>
</body>
</html>
```

The existing EditFormController.java has been modified to support the creation of a new form using a schema. The relevant part is shown below.

```Java
    @RequestMapping(value = "/createAndEditForm", method = RequestMethod.POST)
    public String editForm(Model model, HttpServletRequest request,
    		@RequestParam("schemaFile") MultipartFile file) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	// Is this form schema
    	boolean fromSchema = "on".equals(request.getParameter("fromSchema"));
    	String schemaSource = request.getParameter("schemaSource");
    	String schemaName = request.getParameter("schemaName");
    	String rootXsdFile = request.getParameter("rootXsdFile");

    	formName = request.getParameter("formName");
    	if (formName==null || formName.length()==0) {
            model.addAttribute("formName", formName);
            model.addAttribute("fromSchema", fromSchema);
            model.addAttribute("schemaSource", schemaSource);
            model.addAttribute("schemaName", schemaName);
            model.addAttribute("rootXsdFile", rootXsdFile);
            model.addAttribute("errorMessage", "Form name required");
    		return "new-form";
    	}

    	if (fromSchema && "upload".equals(schemaSource) && file.isEmpty()) {
            model.addAttribute("formName", formName);
            model.addAttribute("fromSchema", fromSchema);
            model.addAttribute("schemaSource", schemaSource);
            model.addAttribute("errorMessage", "A schema zip file must be chosen");
            model.addAttribute("schemaName", schemaName);
            model.addAttribute("rootXsdFile", rootXsdFile);
            return "new-form";
    	}
    	if (schemaName == null || schemaName.length() == 0)
    		schemaName = file.getName();

    	// Now create the app/form
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationEntry appEntry = new ApplicationEntry();
    	// Using form name for both the applicaton name and form name in frevvo
    	appEntry.setTitle(new PlainTextConstruct(formName + APP_SUFFIX));
    	// Insert it on Frevvo
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);
    	appEntry = feed.insert(appEntry);

    	// Create form
    	FormTypeEntry ftEntry = new FormTypeEntry();
    	ftEntry.setTitle(new PlainTextConstruct(formName));
    	ftEntry.setVisibility(Visibility.PUBLICTENANT);
    	// Insert it into Frevvo application
    	ftEntry = appEntry.getFormTypeFeed().insert(ftEntry);
    	assert (ftEntry != null);

        /*********************************************************************/
    	if (fromSchema) {
	    	// add document type
	    	InputStream is = null;
	    	if ("upload".equals(schemaSource))
	    		is = new ByteArrayInputStream(file.getBytes());
	    	else {
			URL xsdUrl = this.getClass().getClassLoader().getResource(XSD_NAME);
			rootXsdFile = "inRootXFullExtension.xsd";
			schemaName = "TestSchema";
			if (xsdUrl != null)
				is = xsdUrl.openStream();
			else
				throw new Exception("Could not open system schema");
	    	}
	    	try {
			// upload schema zip
			SchemaFeed schemaFeed = appEntry.getSchemaFeed();
			SchemaEntry schemaEntry = schemaFeed.insert(Helper.toSchemaMediaSource(is, "application/zip", rootXsdFile, schemaName));
			schemaEntry.setSystem(true);
			schemaEntry = schemaEntry.update();

			// add DocumentTypes to form
			DocumentTypeFeed dtFeed = ftEntry.getDocumentTypeFeed();
			DocumentTypeFeed dtFeedSchema = schemaEntry.getDocumentTypeFeed();
			for (DocumentTypeEntry dtEntry : dtFeedSchema.getEntries()) {
			  dtFeed.insert(dtEntry);
			}
	    	} finally {
	    		is.close();
	    	}
    	}
        /*********************************************************************/

    	FormEntry fEntry = this.createEditingInstance(ftEntry, null, request, fs);
    	assert (fEntry != null);
    	String hRef = getEditLinkUrl(fEntry);

    	// Save the type and instance in this controller which has session scope
    	this.typeHolder = new EntryHolder(ftEntry);
    	this.instanceHolder = new EntryHolder (fEntry);

        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";
    }
```

The code is mostly similar to the previous code introduced in step 2 with the difference being the "fromSchema" snippet marked off with asterisks above. In this code we first add the XSD to the application and then add the root elements from the XSD to the document types of the form. Any uploaded XSD must be provided as a zip file even if it is entirely contained in a single file. However, the system supports zips with multiple XSD files that import each other.

To insert the XSD into the Application entry, we use the method:

```Java
Helper.toSchemaMediaSource(is, "application/zip", rootXsdFile, schemaName);
```

The parameters are the InputStream, the content type (always application/zip), the name of the root XSD in the zip file (if there's a single file, simply provide the name of that file) and finally the name you want to assign to this XSD (can be anything).

Finally, as before, we create an editing instance of the FormType. Now, we have a FormEntry that represents the editing form instance on the frevvo server and a link (URL) to that editing form instance. You should save the FormTypeEntry and FormEntry in your server-side code since you'll need them later. frevvo's API provides a serializable class for this purpose that you'll need to use called EntryHolder.

Lastly, render the form designer embedded in your application's web page using an iframe. This can be seen in edit-form.html and is unchanged from step 2.

This will render your web page with the internal iframe coming from the frevvo server. The internal iframe will display the frevvo form designer embedded. The Data Sources panel of the form designer will display the available document types. The designer can expand and add the desired segment(s) from the XSD.

![schema](http://docs.frevvo.com/d/download/attachments/21533567/data-sources.png?version=1&modificationDate=1536154983000&api=v2 'schema')

Note that in order for frevvo to automatically add document types to the data sources panel in the designer, you may need to alter the schema manually. The top-level element(s) must be annotated using the frevvo:globalElement annotation in order to indicate them to frevvo. A very simple address shema below illustrates this annotation.

```xml
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns="http://www.frevvo.com/schemas/_N_hLoLQsEeiXyeB_VI60Sg"
            xmlns:frevvo="http://www.frevvo.com/appinfo"
            targetNamespace="http://www.frevvo.com/schemas/_N_hLoLQsEeiXyeB_VI60Sg">
   <xsd:element name="form" type="formType">
      <xsd:annotation>
         <xsd:appinfo>
            <frevvo:displaytype/>
            <frevvo:label>form</frevvo:label>
            <frevvo:globalElement>true</frevvo:globalElement>
         </xsd:appinfo>
      </xsd:annotation>
   </xsd:element>
   <xsd:complexType name="formType">
      <xsd:sequence>
         <xsd:element minOccurs="0" name="Street" type="xsd:string">
            <xsd:annotation>
               <xsd:appinfo>
                  <frevvo:displaytype>Text</frevvo:displaytype>
                  <frevvo:label>Street</frevvo:label>
               </xsd:appinfo>
            </xsd:annotation>
         </xsd:element>
         <xsd:element minOccurs="0" name="City" type="xsd:string">
            <xsd:annotation>
               <xsd:appinfo>
                  <frevvo:displaytype>Text</frevvo:displaytype>
                  <frevvo:label>City</frevvo:label>
               </xsd:appinfo>
            </xsd:annotation>
         </xsd:element>
         <xsd:element minOccurs="0" name="State" type="xsd:string">
            <xsd:annotation>
               <xsd:appinfo>
                  <frevvo:displaytype>Text</frevvo:displaytype>
                  <frevvo:label>State</frevvo:label>
               </xsd:appinfo>
            </xsd:annotation>
         </xsd:element>
         <xsd:element minOccurs="0" name="PostalCode" type="xsd:string">
            <xsd:annotation>
               <xsd:appinfo>
                  <frevvo:displaytype>Text</frevvo:displaytype>
                  <frevvo:label>Postal Code</frevvo:label>
               </xsd:appinfo>
            </xsd:annotation>
         </xsd:element>
      </xsd:sequence>
   </xsd:complexType>
</xsd:schema>
```

### Tutorial Step 9: List existing applications / forms

Refer to the /tutorial-step9 sub-directory for all code discussed here and to run the spring boot web application.

It can be useful to show a list of applications (with forms) that are present in frevvo. In this step, we will add a page to show all applications present in frevvo (for the logged in designer user). At the same time, we will move and simplify the test, download and delete operations to be triggered by buttons associated with each of the listed applications (forms) since this is a more natural way to do things.

These are the relevant files in tutorial step 9.

| File                       | Location                                            | Description                                                                                            |
| -------------------------- | --------------------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| app-list.html              | source/main/resoures/templates                      | HTML page that lists the applications found in frevvo with test, download and delete buttons.          |
| ApplicationController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for maniplulating applications in frevvo. **Now supports listing applications**. |

The existing main.html has been modified with a button to list applications.

A new /listApplications end point has been added to ApplicationController.java.

```Java
    @RequestMapping(value = "/listApplications", method = RequestMethod.GET)
    public String listApps(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Get the app feed and display them.  For simplicity, always assuming that each
    	// application contains a single form for right now, so managing forms by managing the app.
    	List<Map<String, String>> apps = new ArrayList<Map<String, String>>();
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		Map<String, String> appInfo = new HashMap<String, String>();
    		apps.add(appInfo);
    		appInfo.put("app-name", appEntry.getTitle().getPlainText());
    		appInfo.put("app-id", appEntry.getId());
		FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
		for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
			// Assuming there will be just one form.
		appInfo.put("form-name", ftEntry.getTitle().getPlainText());
		appInfo.put("form-id", ftEntry.getId());
			break;
		}
    	}

        model.addAttribute("appList", apps);
	return "app-list";
    }
```

Finally, app-list.html lists the apps. Each has a test, download and delete button that front-end existing functionality added in earlier steps.

```HTML
 <html>
<head>
	<title>Application/Form List Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Home</a>
	<h1>OEM (Frevvo) Application/Form Listing Page</h1>

    <table class="data-table">
		<tr>
			<th>Frevvo Application Name</th>
			<th>Frevvo Form Name</th>
			<th>Test</th>
			<th>Download</th>
			<th>Delete</th>
		</tr>
		<tr th:each="app : ${appList}">
			<td th:text=${app.get('app-name')}>--</td>
			<td th:text=${app.get('form-name')}>--</td>
			<td>
				<form method = "POST" action = "testForm">
 					<input name="appId" type="hidden" th:value="${app.get('app-id')}"/>
 					<input name="formName" type="hidden" th:value="${app.get('form-name')}"/>
					<input class="button" type="submit" value="Test Form"/>
				</form>
			</td>
			<td>
				<a th:href="'downloadApp?appId='+ ${app.get('app-id')} +'&formName=' + ${app.get('form-name')}" class="button">Download Application/Form</a>
			</td>
			<td>
				<form method = "POST" action = "deleteApp">
 					<input name="appId" type="hidden" th:value="${app.get('app-id')}"/>
 					<input name="formName" type="hidden" th:value="${app.get('form-name')}"/>
					<input class="button" type="submit" value="Delete Application/Form"/>
				</form>
			</td>
		</tr>
	</table>
</body>
</html>
```

### Tutorial Step 10: Upload an application

Refer to the /tutorial-step10 sub-directory for all code discussed here and to run the spring boot web application.

As discussed above, most OEM partners prefer to save form definitions (metadata) in their own systems. When it's time to use a particular form, you will first try to find it (as described in steps above). If it's not found (usually because it's being used for the first time), you'll need to upload your copy of the application that you downloaded previously.

These are the relevant files in tutorial step 10.

| File                       | Location                                            | Description                                                                                                                               |
| -------------------------- | --------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| app-upload.html            | source/main/resoures/templates                      | Html page with an html form for choosing an application form zip file for upload. There is also an "Overwrite Any Existing App" checkbox. |
| ApplicationController.java | source/main/java/com/frevvo/oemstarter/tutorial/web | Spring MVC controller for maniplulating applications in frevvo. **Now supports uploading applications**.                                  |

After adding an "Upload Application / Form" button in main.html (not shown here), the following code added to ApplicationController.java
handles the form retrieval and the actual application upload.

```Java
    @RequestMapping(value = "/uploadAppForm", method = RequestMethod.GET)
    public String uploadAppFormPage(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
        return "app-upload";
    }

    @RequestMapping(value = "/uploadApp", method = RequestMethod.POST)
    public String uploadApp(Model model, HttpServletRequest request, @RequestParam("applicationFile") MultipartFile file) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	boolean replace = "on".equals(request.getParameter("replace"));

    	if (file.isEmpty()) {
            model.addAttribute("errorMessage", "A application zip file must be chosen");
            model.addAttribute("replace", replace);
            return "app-upload";
    	}

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed appFeed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (appFeed != null);

    	// upload - illustrating multiple ways to do this
    	ApplicationEntry appEntry;
    	if (replace) {
	    	appEntry = appFeed.uploadApplication(new ByteArrayInputStream(file.getBytes()), replace);
    	}
    	else {
	    	try{
    			MediaStreamSource mediaSource = new MediaStreamSource(new ByteArrayInputStream(file.getBytes()), "application/zip");
    			appEntry = appFeed.insert(mediaSource);
    		}catch(ServiceException e) {
    			if(e.getResponseBody().contains("A project with the same ID already exists for this user")) {
    				appFeed.uploadApplication(new ByteArrayInputStream(file.getBytes()), replace,true);
    				//The below two lines of code behaves exactly same as above single line code.
    				/* MediaStreamSource mediaSource = new MediaStreamSource(new ByteArrayInputStream(file.getBytes()), "application/zip;copy=true");
						appEntry = appFeed.insert(mediaSource); */
    			}else {
    				throw e;
    			}
    		}
    	}
    	assert (appEntry != null);

		return "redirect:listApplications";
    }

```

Please note that the code above illustrates two methods of uploading an application: appFeed.uploadApplication and appFeed.insert. The first option (appFeed.uploadApplication) offers a replace parameter that if passed as true can be used to replace any existing application in frevvo that has the exact identifier of that being uploaded. If the replace option is passed as false it behaves the same as the second option (appFeed.insert) and it throws the exception if application with same id already exist. The copy parameter always changes the unique id of the uploaded application so that it always creates a new copy.

After uploading, the code above has an ApplicationEntry (appEntry). You can browse the form type feed of this ApplicationEntry to find your specific form(s) as required.

### Tutorial Step 11: Edit an Existing Form

Refer to the /tutorial-step11 sub-directory for all code discussed here and to run the spring boot web application.

Designer users of the OEM application are likely to want to re-edit an existing form in order to make changes, corrections, etc. We will now add this capability for each application/form that we list from frevvo.

The existing app-list.html is enhanced with a button on each app/form for editing.

```Html
			<td>
				<form method = "POST" action = "editForm">
 					<input name="appId" type="hidden" th:value="${app.get('app-id')}"/>
 					<input name="formName" type="hidden" th:value="${app.get('form-name')}"/>
					<input class="button" type="submit" value="Edit Form"/>
				</form>
			</td>

```

The controller code in EditFormController.java for handling the edit of an existing form is somewhat similar to the create form code from step 2. The major difference is that a new app/form is not created and instead we are finding the target application by its unique id and then the target form type entry by name. Then it is a simple matter to create the editing instance as we have done before.

```Java
    @RequestMapping(value = "/editForm", method = RequestMethod.POST)
    public String editForm2(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application and form to edit.
    	String formName = request.getParameter("formName");
    	String appId = request.getParameter("appId");
    	FormTypeEntry formTypeEntry = null;
    	for (ApplicationEntry entry : feed.getEntries()) {
			if (entry.getId().equals(appId)) {
    			FormTypeFeed ftFeed = entry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formTypeEntry = ftEntry;
    				break;
    			}
				break;
			}
    	}

    	FormEntry fEntry = this.createEditingInstance(formTypeEntry, null, request, fs);
    	assert (fEntry != null);
    	String hRef = getEditLinkUrl(fEntry);

    	// Save the type and instance in this controller which has session scope
    	this.typeHolder = new EntryHolder(formTypeEntry);
    	this.instanceHolder = new EntryHolder (fEntry);

        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";

    }

```

### Tutorial Step 12: Use a form and initialize it with data before rendering it.

Refer to the /tutorial-step12 sub-directory for all code discussed here and to run the spring boot web application.

OEM applications are often designed so that the OEM application stores all submission data from the frevvo form (xml). Also, a common usage scenario is for that submission instance to be edited in frevvo multiple times with multiple round-trips to/from frevvo.

To support this, you can create a form instance in frevvo and initialize it with an XML document.

In order for the tutorial application to properly illustrate this data round-trip to/from frevvo, it first needs to be enhanced to save the data returned from frevvo upon the submit of a runtime form. For our purposes, we identifiy the part of the submission returned from frevvo upon submission by examining the content-type to see if it contains "text/xml". The relevant changes are to submitForm() in UseModeFormController.java.

```Java
    @RequestMapping(value = "/submitUseModeForm", method = RequestMethod.POST)
    public String submitForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	if (instanceHolder != null) {
	    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

	    	FormEntry fEntry = instanceHolder.getEntry(fs, FormEntry.class);
	    	fEntry = fEntry.getSelf();

	    	String actionType = (String)request.getParameter("actionType");
        	if ("cancel".equals(actionType)) {
        		fEntry.cancelInstance();
        		return "redirect:home";
        	}
        	else if ("submit".equals(actionType)) {
		    	FormData data = fEntry.submitInstance(request.getHeader("Authorization"), null);
		    	if (data != null) {

		    		// Save the form data in session so that it can be downloaded later if needed
		    		request.getSession().setAttribute(LAST_FORM_DATA, data);

		    		List<Map<String,Object>> mapData = new ArrayList<Map<String,Object>>();
					for (Part part : data) {
						Map<String,Object> item = new HashMap<String,Object>();
						item.put("contentType", part.getContentType());
						String[] headers = part.getHeader("Content-Disposition");
						item.put("contentDisposition", (headers!=null&&headers.length>0?headers[0]:null));

                        /************  Relevant Changes HERE **************/
						// The text/xml part is the form data payload.  This can be downloaded/saved to re-initialize the form again later.
						item.put("downloadable", false);
						if (part.getContentType().contains("text/xml")) {
							item.put("downloadable", true);
						}
                        /**************************************************/

						mapData.add(item);
						if (part.getContentType().contains("text/xml") || part.getContentType().contains("text/plain")) {
							item.put("data", new BufferedReader(new InputStreamReader(part.getInputStream(), Charset.forName("UTF-8")))
									  .lines().collect(Collectors.joining("\n")));
						}
					}


		            model.addAttribute("formName", request.getParameter("formName"));
		            model.addAttribute("formData", mapData);
		    		return "form-submission-data";
		    	}
        	}
    	}
    	return "redirect:home";
    }

```

The existing form-submission-data.html file is then enhanced with a Download button on the relevant data part (the form submission xml).

```HTML
			<td>
				<a th:if="${part.downloadable}" href="downloadFormData" class="button">Download</a>
			</td>
```

Now the user may choose to download the submission data as a file.

Next we need to enhance the tutorial app to support instantiating a runtime form instance with previously submitted (and downloaded) data. The existing app-list.html is enhanced with a button on each app/form for "testing with data".

```Html
			<td>
				<form method = "POST" action = "editForm">
 					<input name="appId" type="hidden" th:value="${app.get('app-id')}"/>
 					<input name="formName" type="hidden" th:value="${app.get('form-name')}"/>
					<input class="button" type="submit" value="Edit Form"/>
				</form>
			</td>

```

We need an html form for selecting the data file to be used with form instantiation. For this we add a new file: use-form-with-data.html.

```Html
<html>
<head>
	<title>Use Form With Data Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>
</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2>Test Form With Data Initialization</h2>
	<h3 th:text="'Form Name: ' + ${formName}"></h3>

	<form method = "POST" action = "testFormWithData" enctype="multipart/form-data">
		 <input type="hidden" name="appId" th:value="${appId}">
		 <input type="hidden" name="formName" th:value="${formName}">
         <table>
            <tr>
               <span class="error-message" colspan="2" th:text="${errorMessage}"></span>
            </tr>
            <tr>
               <td><label>Form Data XML File</label></td>
               <td><input type="file" name="formDataFile"></td>
            </tr>
            <tr>
               <td></td>
               <td><input class="button" type="submit" value="Test Form"/></td>
            </tr>
         </table>
      </form>
</body>
</html>
```

Finally, the relevant code changes in UseModeFormController.java for serving up the previous form page and for handing the runtime form instantiation with data are as follows.

```Java
    @RequestMapping(value = "/testFormWithData", method = RequestMethod.GET)
    public String testFormWithDataForm(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("appId", request.getParameter("appId"));
        return "use-form-with-data";
    }

    @RequestMapping(value = "/testFormWithData", method = RequestMethod.POST)
    public String testFormWithData(Model model, HttpServletRequest request, @RequestParam("formDataFile") MultipartFile file) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	if (file.isEmpty()) {
            model.addAttribute("formName", request.getParameter("formName"));
            model.addAttribute("appId", request.getParameter("appId"));
            model.addAttribute("errorMessage", "A form data XML file must be chosen");
            return "use-form-with-data";
    	}

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());

    	String formtypeCompositeId = null;
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Find the application we created by id to ensure we have the unique app. Find the form we just created in this application.
    	String appId = request.getParameter("appId");
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		if (appEntry.getId().equals(appId)) {
    			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formtypeCompositeId = ftEntry.getId();
    				break;
    			}
    		}
    	}

    	// Get the formType entry.
    	URL formTypeURL = fs.getEntryURL(FormTypeEntry.class, formtypeCompositeId);
    	assert (formTypeURL != null);
    	final FormTypeEntry ftEntry = (FormTypeEntry) fs.getEntry(formTypeURL, FormTypeEntry.class);
    	assert (ftEntry != null);

    	// Params (optional). There are many parameters - let's set a few.
    	Map<String, Object> params = new HashMap<String, Object>(1);
    	// If set, any form action will be ignored and the document set (including attachments) will be returned when the form is submitted.
    	// Otherwise, normal form action processing is performed (default). We set it so we can retrieve the data later (see below).
    	params.put(FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER, "xml");
    	// Control form resizing. You can set this to
    	// FormTypeEntry.VALUE_RESIZE_TRUE  to turn auto-resizing on.
    	// FormTypeEntry.VALUE_RESIZE_HORIZONTAL to turn auto-resizing on horizontally only.
    	// FormTypeEntry.VALUE_RESIZE_VERTICAL  to turn auto-resizing on vertically only.
    	// Set to FormTypeEntry.VALUE_RESIZE_FALSE
    	params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, FormTypeEntry.VALUE_RESIZE_VERTICAL);
    	// Set the initial width (defaults to the width set in the form or 600px otherwise) for the iframe.
    	params.put(FormTypeEntry.EMBED_WIDTH_PARAMETER, "99%");
    	// Set FormTypeEntry.EMBED_SCROLLING_PARAMETER
    	// to "auto" (default) if you want to automatically show scrollbars if required.
    	// to "no" if you don't want scrollbars.
    	// to "yes" if you always want scrollbars.
    	params.put(FormTypeEntry.EMBED_SCROLLING_PARAMETER, "no");

    	// Get the init document.
    	List<MediaSource> mss = new ArrayList<MediaSource>();
    	InputStream fis = new ByteArrayInputStream(file.getBytes()); // The XML document.
    	MediaStreamSource ms = new MediaStreamSource(fis, "application/xml");
    	ms.setName("Document");
    	mss.add(ms);
    	// You can add as many initial documents as you want to the ArrayList.

    	// Create a new instance of the form, construct its entry and stash it away.
    	URL fUrl = ftEntry.createFormInstance(params, mss, null, null);
    	assert (fUrl != null);
    	String formId = FormsService.SCHEME_KIND_FLOW.equals(ftEntry.getKind()) ? Helper.extractFlowId(fUrl) : Helper.extractFormId(fUrl);
    	assert (formId != null);

    	FormEntry fEntry = null;
    	FormFeed fFeed = ftEntry.getFormFeed();
    	assert (fFeed != null);
    	for (FormEntry fE : fFeed.getEntries()) {
    		if (fE.getId().startsWith(formId)) {
    			fEntry = fE;
    			break;
    		}
    	}
    	assert (fEntry != null);

    	instanceHolder = new EntryHolder (fEntry);  // this controller has session scope
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("formUrl", fEntry.getFormUseEmbedLink().getHref());
        return "test-form";
    }

```

The only real difference is in the createFormInstance() method of the FormTypeEntry. We now pass in the MediaSource list to it. The first and only MediaSource in the list is created from the input stream from the uloaded xml file.

### Tutorial Step 13: Add support for forms that are configured for Captcha.

Refer to the /tutorial-step13 sub-directory for all code discussed here and to run the spring boot web application.

Frevvo forms can be configured to perform a captch challenge/verification prior to submit. The designer has the option to enable captcha on individual forms. This captcha challenge and verification is normally done as part of the submission sequence in the browser when the user clicks the form submit button. When an OEM takes over the submission by providing their own submit button, a call must be made to frevvo prior to submission in order to give frevvo an opportunity to do the captcha challenge and verification.

In order to support the above sequence, frevvo supports posting a 'challenge' message via postMessage to the frame/window containing the frevvo form instance to be submitted. Upon receipt of this message, frevvo does the captcha challenge and verification (gets the captcha token and verifies it) and then reponds back via postMessage with the 'challenge-reponse' message that indicates if verification was required and if verification succeeded. At this point, the actual submission can be done.

You must always make this call/post in order to support frevvo captcha. Frevvo will determine if captcha has been configured for the form and if so, perform the challenge/verification. **Please note that this client-side api support for programmatically invoking recaptcha has only been added to frevvo recently and is only available in frevvo versions 7.6 and 9.1 and later. It is not available in versions 8 and 9.0.**

Building upon the previous tutorial step, we simply modify the test-form-html, which represents the outer OEM page that controls a use-mode form submission. Client-side javascript must be added to handle the submission button click to first trigger the frevvo captcha challenge/verify. Another bit of javascript handles the message coming back from frevvo when the verification is complete and is responsible to the actual form submit.

The changed test-form-html is:

```Html
<html>
<head>
	<title>Test Form Page</title>
	<link rel="stylesheet" type="text/css" href="styles.css"/>

	<script th:inline="javascript">
		function verifyAndSubmit() {
			document.querySelector('input[name=actionType]').value='submit';

			//
			// When it is time to submit, first post the challenge message to the frevvo iFrame
			//  so that it will do any captcha challenge if configured for the form/flow.
			//
			var targetOrigin = /*[[${protocol}+ ${#strings.unescapeJavaScript('://')} + ${hostName} + ':' + ${port}]]*/ 'http://localhost:80';
			document.querySelector('#my-test-form').contentWindow.postMessage({type:"challenge"}, targetOrigin);
			return false;
		}
		function onVerifyChallenge(e) {
			//
			// After the challenge is complete, a 'challenge-response' type message is sent back.
			//  e.data.response = {verificationRequired:true/false, verified:true/false}
			//  If verificationRequired and not verified, then do not submit, otherwise submit
			//
			console.log(e);
			var targetOrigin = /*[[${protocol}+ ${#strings.unescapeJavaScript('://')} + ${hostName} + ':' + ${port}]]*/ 'http://localhost:80';
			if (e.data.type == "challenge-response" && e.origin === targetOrigin)  {
				if (!e.data.response.verificationRequired || e.data.response.verified)
					document.querySelector('#submitForm').submit();
			}
		}
		window.addEventListener("message", onVerifyChallenge);
	</script>

</head>
<body>
	<a href="home"><< Back</a>
	<h1>OEM Outer Page</h1>
	<h2 th:text="'Testing Form: ' + ${formName}">Testing Form: Unknown</h2>

 	<form id="submitForm" method = "POST" action = "submitUseModeForm">
 		<input name="actionType" type="hidden"/>
 		<input name="formName" type="hidden" th:value="${formName}"/>
 		<input class="button" type="submit" value="Cancel Form" onclick="document.querySelector('input[name=actionType]').value='cancel';document.querySelector('#submitForm').submit();" />
 		<input class="button" type="submit" value="Submit Form" onclick="return verifyAndSubmit();" />
  	</form>
	<div id="wrapper">
	    <!-- Give the frevvo generated iFrame a specific id that we can use to find it -->
		<script th:src="${formUrl}+'&id=my-test-form'" type="text/javascript"></script>
	</div>

</body>
</html>
```

Frevvo does provide an alternate to using postMessage as shown above. It is a direct call into a frevvo javascript method that can be used when you know you do not have any cross domain issues.

```Javascript
_frevvo.api.forms.captcha.clientChallenge(callbackFunction);    // callbackFunction(resultsObj) - resultsObj: {verificationRequired: true/false, verified: true/false}
```

### Congratulations

You are now armed with most of what you need to build a typical integration with frevvo Live Forms.
