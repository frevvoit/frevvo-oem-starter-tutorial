package com.frevvo.oemstarter.tutorial.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frevvo.forms.client.ApplicationEntry;
import com.frevvo.forms.client.ApplicationFeed;
import com.frevvo.forms.client.EntryHolder;
import com.frevvo.forms.client.FormEntry;
import com.frevvo.forms.client.FormTypeEntry;
import com.frevvo.forms.client.FormsService;
import com.frevvo.forms.client.Helper;
import com.frevvo.forms.client.ext.Visibility;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.util.ServiceException;

@Controller
@Scope("session")
public class EditFormController {
	private EntryHolder typeHolder;
	private EntryHolder instanceHolder;
	static final public String AUTHORIZATION_HEADER_NAME = "Authorization";
	static final private String iframeId = "12345";
	
	private String formName;
	
    @RequestMapping(value = "/newForm", method = RequestMethod.GET)
    public String newEditFormPage(Model model, HttpSession session) {
    	if (!MainController.isLoggedIn(session))
    		return "redirect:login";
        return "new-form";
    }

    @RequestMapping(value = "/createAndEditForm", method = RequestMethod.POST)
    public String editForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	
    	formName = request.getParameter("formName");
    	if (formName==null || formName.length()==0)
    		return "redirect:newForm";
    	
    	// Now create the app/form
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	ApplicationEntry appEntry = new ApplicationEntry();
    	// Using form name for both the applicaton name and form name in frevvo
    	appEntry.setTitle(new PlainTextConstruct(formName + " (App)"));
    	// Insert it on Frevvo
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);
    	appEntry = feed.insert(appEntry);

    	// Create form
    	FormTypeEntry ftEntry = new FormTypeEntry();
    	ftEntry.setTitle(new PlainTextConstruct(formName));
    	ftEntry.setVisibility(Visibility.PUBLICTENANT);
    	// Insert it into Frevvo application
    	ftEntry = appEntry.getFormTypeFeed().insert(ftEntry);
    	assert (ftEntry != null);

    	FormEntry fEntry = this.createEditingInstance(ftEntry, null, request, fs);
    	assert (fEntry != null);
    	String hRef = getEditLinkUrl(fEntry);

    	// Save the type and instance in this controller which has session scope
    	this.typeHolder = new EntryHolder(ftEntry);
    	this.instanceHolder = new EntryHolder (fEntry);
    	
        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";
    }
    
    protected FormEntry createEditingInstance(FormTypeEntry myEntry,
			Map<String, Object> params, HttpServletRequest req, FormsService formsService)
			throws ServiceException, MalformedURLException, IOException {
		if (params == null)
			params = new HashMap<String, Object>();
		params.put("edit", true);

		URL fUrl = myEntry.createFormInstance(params, null,
				req.getHeader(AUTHORIZATION_HEADER_NAME), null);
		assert (fUrl != null);
		
		// ATTEMPT BASED ON PATTERNS IN FormTypeEntry.java
		String entryId = Helper.getFormEntryIdFromInstance(fUrl);
		URL entryUrl = formsService.getEntryURL(FormEntry.class, entryId);
		return formsService.getEntry(entryUrl, FormEntry.class);
	}
 
    /**
     * Entry point for Saving, Finishing (and Saving), and Canceling a form edit.
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/submitForm", method = RequestMethod.POST)
    public String submitForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String actionType = (String)request.getParameter("actionType");
    	
    	if ("save".equals(actionType))
    		return saveEditingForm(model, request);
    	else if ("finish".equals(actionType))
    		return saveAndFinishEditingForm(request);
    	else if ("cancel".equals(actionType))
    		return cancelEditingForm(request);
    	
    	return "redirect:home";   // should not get here
    }
    
    private String saveEditingForm(Model model, HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(true, request.getHeader(AUTHORIZATION_HEADER_NAME), null);

    	// Saved earlier in typeHolder
    	FormTypeEntry ftEntry = this.typeHolder.getEntry(fs, FormTypeEntry.class);
    	assert (ftEntry != null);
    	fEntry = createEditingInstance(ftEntry, null, request, fs);
    	assert (fEntry != null);
    	String hRef = getEditLinkUrl(fEntry);

    	// Stash it away.    	
    	EntryHolder instanceHolder = new EntryHolder (fEntry);
    
    	// re-render the editing form
        model.addAttribute("formName", formName);
        model.addAttribute("iframeId", iframeId);
        model.addAttribute("iframeSrc", hRef);
        return "edit-form";
    }
    
    private String saveAndFinishEditingForm(HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(true, request.getHeader(AUTHORIZATION_HEADER_NAME), null);
    	return "redirect:home";
    }
    
    private String cancelEditingForm(HttpServletRequest request) throws Exception {
    	// Saved above in instanceHolder
    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	FormEntry fEntry = this.instanceHolder.getEntry(fs, FormEntry.class);
    	fEntry = fEntry.getSelf();
    	fEntry.submitEditingInstance(false, request.getHeader(AUTHORIZATION_HEADER_NAME), null);
    	return "redirect:home";
    }
    
    private String getEditLinkUrl(FormEntry fEntry) {
		Map<String, Object> params = new HashMap<String, Object>(4);
		// You can add any URL params here.
		params.put(FormTypeEntry.EMBED_EXT_SUBMIT_PARAMETER, true);
		params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, true);
		params.put("_iframe_id", iframeId);
		params.put("_referrer_url", "blah");
    	return fEntry.getFormEditLink(params).getHref();
    }
    
}
