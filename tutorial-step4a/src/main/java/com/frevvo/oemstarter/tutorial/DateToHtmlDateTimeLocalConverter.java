package com.frevvo.oemstarter.tutorial;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class DateToHtmlDateTimeLocalConverter implements Converter<Date, String>{
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
	
	@Override
	public String convert(Date source) {
		return sdf.format(source);
	}

}
