package com.frevvo.oemstarter.tutorial;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

public class HtmlDateTimeLocalConverter implements Converter<String, Date>{
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

	@Override
	public Date convert(String source) {
		if (StringUtils.isEmpty(source)) return null;
		try {
			return sdf.parse(source);
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

}
