package com.frevvo.oemstarter.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class TutorialApplication implements WebMvcConfigurer{
	
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new HtmlDateTimeLocalConverter());
		registry.addConverter(new DateToHtmlDateTimeLocalConverter());
	}

	public static void main(String[] args) {
		SpringApplication.run(TutorialApplication.class, args);
	}
}
