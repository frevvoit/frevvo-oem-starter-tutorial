package com.frevvo.oemstarter.tutorial.web;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frevvo.forms.client.ApplicationEntry;
import com.frevvo.forms.client.ApplicationFeed;
import com.frevvo.forms.client.EntryHolder;
import com.frevvo.forms.client.FormEntry;
import com.frevvo.forms.client.FormFeed;
import com.frevvo.forms.client.FormTypeEntry;
import com.frevvo.forms.client.FormTypeFeed;
import com.frevvo.forms.client.FormsService;
import com.frevvo.forms.client.Helper;

@Controller
@Scope("session")
public class UseModeFormController {

	private EntryHolder instanceHolder;
	
    @RequestMapping(value = "/testForm", method = RequestMethod.GET)
    public String testFormSelectionPage(Model model, HttpServletRequest request) {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";
    	String lastFormName = request.getParameter("lastFormName");
    	if (lastFormName == null)
    		lastFormName = "";
        model.addAttribute("lastFormName", lastFormName);
        return "test-form-selection";
    }

    @RequestMapping(value = "/testForm", method = RequestMethod.POST)
    public String testForm(Model model, HttpServletRequest request) throws Exception {
    	if (!MainController.isLoggedIn(request.getSession()))
    		return "redirect:login";

    	FormsService fs = (FormsService)MainController.getFormsService(request.getSession());
    	
    	String formtypeCompositeId = null;
    	ApplicationFeed feed = fs.getFeed(fs.getFeedURL(ApplicationFeed.class), ApplicationFeed.class);
    	assert (feed != null);

    	// Get the FormTypeEntry so you can create an instance of it. Earlier, we used the entry stashed away in a holder.
    	// Here's another way to construct a FormTypeEntry for an existing form (the one we just created above).

    	// Find the application we created by name (you can also find it by ID). Find the form we just created in this application.
    	String appName = request.getParameter("formName") + EditFormController.APP_SUFFIX;
    	for (ApplicationEntry appEntry : feed.getEntries()) {
    		if (appEntry.getTitle().getPlainText().contains(appName)) {
    			FormTypeFeed ftFeed = appEntry.getFormTypeFeed();
    			for (FormTypeEntry ftEntry : ftFeed.getEntries()) {
    				// There will be just one form.
    				formtypeCompositeId = ftEntry.getId();
    				break;
    			}
    		}
    	}

    	
    	// Get the formType entry.
    	URL formTypeURL = fs.getEntryURL(FormTypeEntry.class, formtypeCompositeId);
    	assert (formTypeURL != null);
    	final FormTypeEntry ftEntry = (FormTypeEntry) fs.getEntry(formTypeURL, FormTypeEntry.class);
    	assert (ftEntry != null);    
    
    	// Params (optional). There are many parameters - let's set a few.
    	Map<String, Object> params = new HashMap<String, Object>(1);
    	// If set, any form action will be ignored and the document set (including attachments) will be returned when the form is submitted.
    	// Otherwise, normal form action processing is performed (default). We set it so we can retrieve the data later (see below).
    	params.put(FormTypeEntry.FORMTYPE_FORMACTIONDOCS_PARAMETER, "xml");
    	// Control form resizing. You can set this to
    	// FormTypeEntry.VALUE_RESIZE_TRUE  to turn auto-resizing on.
    	// FormTypeEntry.VALUE_RESIZE_HORIZONTAL to turn auto-resizing on horizontally only.
    	// FormTypeEntry.VALUE_RESIZE_VERTICAL  to turn auto-resizing on vertically only.
    	// Set to FormTypeEntry.VALUE_RESIZE_FALSE
    	params.put(FormTypeEntry.EMBED_RESIZE_PARAMETER, FormTypeEntry.VALUE_RESIZE_VERTICAL);
    	// Set the initial width (defaults to the width set in the form or 600px otherwise) for the iframe.
    	params.put(FormTypeEntry.EMBED_WIDTH_PARAMETER, "99%");
    	// Set FormTypeEntry.EMBED_SCROLLING_PARAMETER
    	// to "auto" (default) if you want to automatically show scrollbars if required.
    	// to "no" if you don't want scrollbars.
    	// to "yes" if you always want scrollbars.
    	params.put(FormTypeEntry.EMBED_SCROLLING_PARAMETER, "no");    	
    
    	// Create a new instance of the form, construct its entry and stash it away.
    	FormEntry fEntry = ftEntry.createInstance(params, null);
    	assert (fEntry != null);

    	instanceHolder = new EntryHolder (fEntry);  // this controller has session scope
        model.addAttribute("formName", request.getParameter("formName"));
        model.addAttribute("formUrl", fEntry.getFormUseEmbedLink().getHref());
        return "test-form";
    	
    }
    
}
