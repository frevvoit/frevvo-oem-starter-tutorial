package com.frevvo.oemstarter.tutorial.web;

import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.frevvo.forms.client.FormsService;
import com.frevvo.forms.client.util.AutoLoginUserInfo;
import com.frevvo.oemstarter.tutorial.model.LoginInfo;
import com.google.gdata.util.ServiceException;

@Controller
@Scope("session")
public class MainController {
	static final public String FORMS_SERVICE_ATTR = "frevvo.forms.service";
	static final public String LOGIN_AS_USER_ATTR = "frevvo.login.as.user";
	static final public String LAST_FORM_NAME = "frevvo.forms.last.form.name";
	
    @Value("${frevvo.default.protocol}")
    String defaultProtocol;
    @Value("${frevvo.default.port}")
    String defaultPort;
    @Value("${frevvo.default.hostName}")
    String defaultHostName;
    @Value("${frevvo.default.userId}")
    String defaultUserId;
    @Value("${frevvo.default.tenantAdminUserId}")
    String defaultTenantAdminUserId;
    @Value("${frevvo.default.tenantAdminPassword}")
    String defaultTenantAdminPassword;
    
    
    private String loginAsUser;

    @RequestMapping(value= {"/", "/home"})
    public String homePage(Model model, HttpSession session) {
    	if (!MainController.isLoggedIn(session))
    		return "redirect:login";
    	FormsService fs = MainController.getFormsService(session);
        model.addAttribute("loginUser", fs.getUsername());
        model.addAttribute("loginAsUser", MainController.getLoginAsUser(session));
        model.addAttribute("lastFormName", session.getAttribute(LAST_FORM_NAME)!=null?session.getAttribute(LAST_FORM_NAME):"");
        return "main";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model, HttpSession session) {
    	if (!MainController.isLoggedIn(session)) {
	        model.addAttribute("protocol", defaultProtocol);
	        model.addAttribute("port", defaultPort);
	        model.addAttribute("hostName", defaultHostName);
	        model.addAttribute("userId", defaultUserId);
	        model.addAttribute("tenantAdminUserId", defaultTenantAdminUserId);
	        model.addAttribute("tenantAdminPassword", defaultTenantAdminPassword);
	        return "login";
    	}
    	else {
        	FormsService fs = MainController.getFormsService(session);
	        model.addAttribute("loginUser", fs.getUsername());
	        model.addAttribute("loginAsUser", MainController.getLoginAsUser(session));
	        return "main";
    	}
    }
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("SpringWeb")LoginInfo loginInfo, Model model, HttpSession session) {
    	FormsService fs = null;
    	if (MainController.isLoggedIn(session)) {
    		fs = MainController.getFormsService(session);
	        model.addAttribute("loginUser", fs.getUsername());
	        model.addAttribute("loginAsUser", MainController.getLoginAsUser(session));
	        return "main";
    	}
    	// Else loginAs now
		try {
			fs = new FormsService(loginInfo.getProtocol(), loginInfo.getHostName(), loginInfo.getPort(), null);
			MainController.setFormsService(session, fs);
			setFrevvoProtocol(loginInfo.getProtocol());
			setFrevvoPort(loginInfo.getPort());
			setFrevvoHostName(loginInfo.getHostName());
		} catch (MalformedURLException e1) {
			// Failed
			this.initializeLoginInfoAttributes(loginInfo, model);
			return "login";  // re-display
		}
    	Map<String, String> customParams = new HashMap<String, String>(1);
    	customParams.put("autoLogin", "true");

    	// If you want to login as a designer user (who can create forms), pass in this role. Otherwise, pass in null.
    	List<String> roles = Collections.singletonList("frevvo.Designer");

    	AutoLoginUserInfo alui = null;
		try {
			alui = fs.loginAs(loginInfo.getUserId(), loginInfo.getTenantAdminUserId(), loginInfo.getTenantAdminPassword(), 
					          true, roles, loginInfo.getFirstName(), loginInfo.getLastName(), 
					          loginInfo.getEmailAddress(), customParams);
		} catch (ServiceException e) {
		}
    	if (alui == null) {
    		// LoginAs failed, re-display login
    		this.initializeLoginInfoAttributes(loginInfo, model);
	        return "login";
    	}
    	MainController.setLoginAsUser(session,  alui.userId);
    	
    	// logged in
        return "redirect:home";
    }
    private void initializeLoginInfoAttributes(LoginInfo loginInfo, Model model) {
        model.addAttribute("protocol", loginInfo.getProtocol());
        model.addAttribute("port", loginInfo.getPort());
        model.addAttribute("hostName", loginInfo.getHostName());
        model.addAttribute("userId", loginInfo.getUserId());
        model.addAttribute("tenantAdminUserId", loginInfo.getTenantAdminUserId());
        model.addAttribute("tenantAdminPassword", loginInfo.getTenantAdminPassword());
        model.addAttribute("lastName", loginInfo.getLastName());
        model.addAttribute("firstName", loginInfo.getFirstName());
        model.addAttribute("emailAddress", loginInfo.getEmailAddress());
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(Model model, HttpSession session) throws ServiceException {
    	FormsService fs = MainController.getFormsService(session);
    	if (fs != null) {
    		fs.logout();
			MainController.setFormsService(session, null);
    	}
        return "redirect:home";
    }
    
    public static FormsService getFormsService(HttpSession session) {
    	return (FormsService)session.getAttribute(FORMS_SERVICE_ATTR);    	
    }
    public static void setFormsService(HttpSession session, FormsService fs) {
    	session.setAttribute(FORMS_SERVICE_ATTR, fs);    	
    }
    public static String getLoginAsUser(HttpSession session) {
    	return (String)session.getAttribute(LOGIN_AS_USER_ATTR);    	
    }
    public static void setLoginAsUser(HttpSession session, String user) {
    	session.setAttribute(LOGIN_AS_USER_ATTR, user);    	
    }
    public static boolean isLoggedIn(HttpSession session) {
    	FormsService fs = MainController.getFormsService(session);
    	return (fs != null && fs.getUsername() != null);
    }
    private static String frevvoProtocol;
    private static String frevvoHostName;
    private static Integer frevvoPort;

	public static String getFrevvoProtocol() {
		return frevvoProtocol;
	}

	public static void setFrevvoProtocol(String frevvoProtocol) {
		MainController.frevvoProtocol = frevvoProtocol;
	}

	public static String getFrevvoHostName() {
		return frevvoHostName;
	}

	public static void setFrevvoHostName(String frevvoHostName) {
		MainController.frevvoHostName = frevvoHostName;
	}

	public static Integer getFrevvoPort() {
		return frevvoPort;
	}

	public static void setFrevvoPort(Integer frevvoPort) {
		MainController.frevvoPort = frevvoPort;
	}
    
    
}