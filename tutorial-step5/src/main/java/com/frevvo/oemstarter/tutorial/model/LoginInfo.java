package com.frevvo.oemstarter.tutorial.model;

public class LoginInfo {
	private String protocol = "http";
	private String hostName;
	private Integer port;
	private String userId;
	private String tenantAdminUserId;
	private String tenantAdminPassword;
	private String firstName;
	private String lastName;
	private String emailAddress;
	
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTenantAdminUserId() {
		return tenantAdminUserId;
	}
	public void setTenantAdminUserId(String tenantAdminUserId) {
		this.tenantAdminUserId = tenantAdminUserId;
	}
	public String getTenantAdminPassword() {
		return tenantAdminPassword;
	}
	public void setTenantAdminPassword(String tenantAdminPassword) {
		this.tenantAdminPassword = tenantAdminPassword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	
}
